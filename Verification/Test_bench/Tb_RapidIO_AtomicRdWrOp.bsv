/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO_AtomicRdWrOp Testbench
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO atomic Read Write Operation. 
-- 1. Atomic Nread Operation.
-- 2. Atomic Increment Operation.
-- 3. Atomic Decrement Operation. 
-- 4. Atomic Set Operation. 
-- 5. Atomic Clear Operation.
-- 6. Atomic NWrite Operation( With and without Response ).
-- 7. Atomic Swap Operation. 
-- 8. Atomic Test and Swap Operation. 
-- 9. Test cases for bytecount and byte_en.
--
-- To Do's
-- Atomic Compare and Swap (ttpye =4'b1101) - not yet implemented
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
package Tb_RapidIO_AtomicRdWrOp;


import RapidIO_AtomicRdWrOp ::* ;
import RapidIO_DTypes ::* ;
import RapidIO_FTypeDfns ::* ;


module mkTb_for_RapidIO_AtomicRdWrOp(Empty);

// interface

Ifc_RapidIO_AtomicRdWrOp atomicrwop <- mkRapidIO_AtomicRdWrOp;


Wire#(Bool) wr_outputs_tresp_sof <- mkDWire (False);
Wire#(Bool) wr_outputs_tresp_eof <- mkDWire (False);
Wire#(Bool) wr_outputs_tresp_vld <- mkDWire (False);
Wire#(TT) wr_outputs_tresp_tt <- mkDWire (0);
Wire#(Data) wr_outputs_tresp_data <- mkDWire (0);
Wire#(Bool) wr_outputs_tresp_crf <- mkDWire (False);
Wire#(Prio) wr_outputs_tresp_prio <- mkDWire (0);
Wire#(Type) wr_outputs_tresp_ftype <- mkDWire (0);
Wire#(SourceId) wr_outputs_tresp_src_id <- mkDWire (0);
Wire#(DestId) wr_outputs_tresp_dest_id <- mkDWire (0);
Wire#(Status) wr_outputs_tresp_status <- mkDWire (0);
Wire#(TranId) wr_outputs_tresp_tid <- mkDWire (0);
Wire#(Type) wr_outputs_tresp_ttype <- mkDWire (0);

Wire#(Bool) sof <- mkDWire (False);
Wire#(Bool) eof <- mkDWire (False);
Wire#(Bool) vld <- mkDWire (False);
Wire#(TT) tt <- mkDWire (0);
Wire#(Data) data <- mkDWire (0);
Wire#(Bool) crf <- mkDWire (False);
Wire#(Prio) prio <- mkDWire (0);
Wire#(Type) ftype <- mkDWire (0);
Wire#(SourceId) src_id <- mkDWire (0);
Wire#(DestId) dest_id <- mkDWire (0);
Wire#(Status) status <- mkDWire (0);
Wire#(TranId) tid <- mkDWire (0);
Wire#(Type) ttype <- mkDWire (0);
Wire#(Addr) addr <- mkDWire (0);
Wire#(ByteCount) byte_count <- mkDWire (0);
Wire#(ByteEn) byte_en <- mkDWire (0);


// Clock Declaration

Reg#(Bit#(7)) reg_ref_clk <- mkReg (0);		


///  clock

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 64)
	$finish (0);
endrule

rule rl_input0(reg_ref_clk == 0);
	atomicrwop._inputs_treq_sof(False);			// Sof is false hence there wont be any output data
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(64'h0000000000000000);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr(50'h000000008);
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b0100);
	atomicrwop._inputs_treq_byte_count(0);
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation
rule rl_in0(reg_ref_clk == 3);
	$display ("\n \n For 32 bit device  - since sof is false there wont be any valid target reponse signals" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=64'h0000000000000000;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b001;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b0100;	
	addr<=50'h000000008;
	byte_count<=0;		   
	byte_en<=0;	    
endrule

/* Clock pulses 1 or 2 or 3 cant be used since it will over write the previous data. So an interval for 4 is required between each input 
rule rl_input0(reg_ref_clk == 1 );
	$display ("\n \n For 32 bit device " );
	atomicrwop._inputs_treq_sof(False);			// Sof is false hence there wont be any output data
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(64'h0000000000000000);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr(50'h000000008);
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b0100);
	atomicrwop._inputs_treq_byte_count(0);
	atomicrwop._inputs_treq_byte_en(0);     
endrule
*/

rule rl_input1(reg_ref_clk == 4);		   // the status will be shown error since ftype = 5 and ttype =1101(which is not implemented)
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(64'h9999999999999999);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0101);
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr(50'h000000009);
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1101);
	atomicrwop._inputs_treq_byte_count(0);
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation
rule rl_in1(reg_ref_clk == 7);
	$display ("\n \n For 32 bit device - Here ftype = 0101 and ttype = 1101(which is not implemented)" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=64'h9999999999999999;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0101;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b1101;	
	addr<=50'h000000009;
	byte_count<=0;		   
	byte_en<=0;	    
endrule

// Read operation
rule rl_input2(reg_ref_clk == 8);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr('d1);		      	// Addresses to the first line of memory.hex file
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b0100);		    	// Nread operation 
	atomicrwop._inputs_treq_byte_count('d8);	  	// Eight bytes corresponds to byte_en 
	atomicrwop._inputs_treq_byte_en(8'b11111111);    	// This field means the whole 64bit data in memory 
endrule

// The above target request signals are stored in wires to display during simulation
rule rl_in2(reg_ref_clk == 11);
	$display ("\n \n For 32 bit device - Nread operation" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='h1;
	byte_count<='d8;		   
	byte_en<=8'b11111111;	    
endrule

// Atomic increment operation
rule rl_input3(reg_ref_clk == 12);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr('d8);			// Addresses to the 2nd line of memory.hex file
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1100);			// Atomic increment operation
	atomicrwop._inputs_treq_byte_count('d8);		// Eight bytes corresponds to byte_en 
	atomicrwop._inputs_treq_byte_en(8'b11111111);		// This feild means the whole 64bit data in memory
endrule

// The above target request signals are stored in wires to display during simulation
rule rl_in3(reg_ref_clk == 15);
	$display ("\n \n For 32 bit device - Atomic increment operation" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d8;
	byte_count<='d8;		   
	byte_en<=8'b11111111;	    
endrule

// Atomic decrement operation //
rule rl_input4(reg_ref_clk == 16);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr('d16);		      	// Addresses to the 3rd line of memory.hex file
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1101);		    	// Atomic decrement operation
	atomicrwop._inputs_treq_byte_count('d8);	   	// Eight bytes corresponds to byte_en 
	atomicrwop._inputs_treq_byte_en(8'b11111111);     	// This feild means the whole 64bit data in memory
endrule


// The above target request signals are stored in wires to display during simulation //
rule rl_in4(reg_ref_clk == 19);
	$display ("\n \n For 32 bit device - Atomic decrement operation" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d16;
	byte_count<='d8;		   
	byte_en<=8'b11111111;	    
endrule

// Atomic set operation
rule rl_input5(reg_ref_clk == 20);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr('d24);		      	// Addresses to the 4th line of memory.hex file
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1110);		    	// Atomic set operation
	atomicrwop._inputs_treq_byte_count('d8);	   	// Eight bytes corresponds to byte_en 
	atomicrwop._inputs_treq_byte_en(8'b11111111);     	// This feild means the whole 64bit data in memory 
endrule

// The above target request signals are stored in wires to display during simulation
rule rl_in5(reg_ref_clk == 23);
	$display ("\n \n For 32 bit device - Atomic set operation" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d24;
	byte_count<='d8;		   
	byte_en<=8'b11111111;	    
endrule

// Atomic clear operation 
rule rl_input6(reg_ref_clk == 24);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr('d32);		      	// Addresses to the 5th line of memory.hex file
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1111);		    	// Atomic clear operation 
	atomicrwop._inputs_treq_byte_count('d8);	   	// Eight bytes corresponds to byte_en  
	atomicrwop._inputs_treq_byte_en(8'b11111111);     	// This feild means the whole 64bit data in memory 
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in6(reg_ref_clk == 27);
	$display ("\n \n For 32 bit device - Atomic clear operation" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d32;
	byte_count<='d8;		   
	byte_en<=8'b11111111;	    
endrule

// Atomic clear operation 
rule rl_input7(reg_ref_clk == 28);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b01);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation 
	atomicrwop._inputs_treq_dest_id(32'h268c0000);
	atomicrwop._inputs_treq_source_id(32'h568c0000);
	atomicrwop._inputs_treq_addr('d40);		      	// Addresses to the 6th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1111);		    	// Atomic clear operation 
	atomicrwop._inputs_treq_byte_count('d3);           	// Three bytes corresponds to byte_en  
	atomicrwop._inputs_treq_byte_en(8'b00000111);     	// This feild means last three bytes of 64bit data in memory 
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in7(reg_ref_clk == 31);
	$display ("\n \n For 16 bit device - Atomic clear operation and three bytes of memory is modified according to byte_en" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b01;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'h268c0000;
	src_id<=32'h568c0000;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d40;
	byte_count<='d3;		   
	byte_en<=8'b00000111;	    
endrule

// Atomic set operation 
rule rl_input8(reg_ref_clk == 32);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b00);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation 
	atomicrwop._inputs_treq_dest_id(32'h8c000000);
	atomicrwop._inputs_treq_source_id(32'h4c000000);
	atomicrwop._inputs_treq_addr('d48);		      	// Addresses to the 7th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1110);		    	// Atomic set operation 
	atomicrwop._inputs_treq_byte_count('d2);	   	// Two bytes corresponds to byte_en  
	atomicrwop._inputs_treq_byte_en(8'b00001100);     	// This feild means middle two bytes of 64bit data in memory 
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in8(reg_ref_clk == 35);
	$display ("\n \n For 8 bit device - Atomic set operation and two bytes of memory is modified according to byte_en" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b00;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'h8c000000;
	src_id<=32'h4c000000;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d48;
	byte_count<='d2;		   
	byte_en<=8'b00001100;	    
endrule

// Write operation 
rule rl_input9(reg_ref_clk == 36);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b00);
	atomicrwop._inputs_treq_data(64'h9999999988999999);	// Data to be written 
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0101);			// Write operation 
	atomicrwop._inputs_treq_dest_id(32'h8c000000);
	atomicrwop._inputs_treq_source_id(32'h4c000000);
	atomicrwop._inputs_treq_addr('d56);		     	// Addresses to the 8th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b0100);		    	// Nwrite operation 
	atomicrwop._inputs_treq_byte_count(0);	  
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in9(reg_ref_clk == 39);
	$display ("\n \n For 8 bit device - nwrite operation");
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b00;
	data<=64'h9999999988999999;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0101;			
	dest_id<=32'h8c000000;
	src_id<=32'h4c000000;
	tid<=8'hbf;
	ttype<=4'b0100;	
	addr<='d56;
	byte_count<=0;		   
	byte_en<=0;	    
endrule

// Write operation with response 
rule rl_input10(reg_ref_clk == 40);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b01);
	atomicrwop._inputs_treq_data(64'h9999999988999999);	// Data to be written 
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0101);			// Write operation
	atomicrwop._inputs_treq_dest_id(32'h568c0000);
	atomicrwop._inputs_treq_source_id(32'h468c0000);
	atomicrwop._inputs_treq_addr('d64);		      	// Addresses to the 9th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b0101);		    	// NWrite with response operation
	atomicrwop._inputs_treq_byte_count(0);	  
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in10(reg_ref_clk == 43);
	$display ("\n \n For 16 bit device - nwrite with response operation");
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b01;
	data<=64'h9999999988999999;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0101;			
	dest_id<=32'h568c0000;
	src_id<=32'h468c0000;
	tid<=8'hbf;
	ttype<=4'b0101;	
	addr<='d64;
	byte_count<=0;		   
	byte_en<=0;	    
endrule

// Write operation with Atomic Swap //
rule rl_input11(reg_ref_clk == 44);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(64'h8889999988999888);	// Data to be written 
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0101);			// Write operation 
	atomicrwop._inputs_treq_dest_id(32'h1234568c);
	atomicrwop._inputs_treq_source_id(32'h1234468c);
	atomicrwop._inputs_treq_addr('d72);		      	// Addresses to the 10th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1100);		    	// Atomic swap operation 
	atomicrwop._inputs_treq_byte_count(0);	  
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in11(reg_ref_clk == 47);
	$display ("\n \n For 32 bit device - Atomic swap operation");
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=64'h8889999988999888;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0101;			
	dest_id<=32'h1234568c;
	src_id<=32'h1234468c;
	tid<=8'hbf;
	ttype<=4'b1100;	
	addr<='d72;
	byte_count<=0;		   
	byte_en<=0;	    
endrule


 // Write operation with Atomic test and Swap  where memory data = 0
rule rl_input12(reg_ref_clk == 48);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(64'h8888888888888888);	// Data to be written 
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0101);			// Write operation 
	atomicrwop._inputs_treq_dest_id(32'h1234568c);
	atomicrwop._inputs_treq_source_id(32'h1234468c);
	atomicrwop._inputs_treq_addr('d80);		      	// Addresses to the 11th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1110);		    	// Atomic test and swap operation 
	atomicrwop._inputs_treq_byte_count(0);	  
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in12(reg_ref_clk == 51);
	$display ("\n \n For 32 bit device - Atomic test and swap operation");
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=64'h8888888888888888;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0101;			
	dest_id<=32'h1234568c;
	src_id<=32'h1234468c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d80;
	byte_count<=0;		   
	byte_en<=0;	    
endrule

 // Write operation with Atomic test and Swap  where memory data != 0 
rule rl_input13(reg_ref_clk == 52);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(64'h8888888888888888);	// Data to be written 
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0101);			// Write operation 
	atomicrwop._inputs_treq_dest_id(32'h1234568c);
	atomicrwop._inputs_treq_source_id(32'h1234468c);
	atomicrwop._inputs_treq_addr('d88);		      	// Addresses to the 12th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1110);		    	// Atomic test and swap operation 
	atomicrwop._inputs_treq_byte_count(0);	  
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in13(reg_ref_clk == 55);
	$display ("\n \n For 32 bit device - Atomic test and swap operation");
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=64'h8888888888888888;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0101;			
	dest_id<=32'h1234568c;
	src_id<=32'h1234468c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d88;
	byte_count<=0;		   
	byte_en<=0;	    
endrule

 // Ftype 3 is given inorder to check the functionality. There should not be any target response 
rule rl_input14(reg_ref_clk == 56);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(64'h8888888888888888);	// Data to be written 
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0011);			// ftype 3
	atomicrwop._inputs_treq_dest_id(32'h1234568c);
	atomicrwop._inputs_treq_source_id(32'h1234468c);
	atomicrwop._inputs_treq_addr('d88);			// Addresses to the 12th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b0110);		   
	atomicrwop._inputs_treq_byte_count(0);	  
	atomicrwop._inputs_treq_byte_en(0);     
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_in14(reg_ref_clk == 59);
	$display ("\n \n For 32 bit device - ftype 3 (Hence there is no target reponse) ");
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=64'h8888888888888888;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0101;			
	dest_id<=32'h1234568c;
	src_id<=32'h1234468c;
	tid<=8'hbf;
	ttype<=4'b1110;	
	addr<='d88;
	byte_count<=0;		   
	byte_en<=0;	    
endrule


// This rule is used to check whether memory is updated or not 
// Atomic clear operation has done on 5th line already (reg_ref_clk == 24). Now atomic increment is applied. 
rule rl_update(reg_ref_clk == 60);
	atomicrwop._inputs_treq_sof(True);
	atomicrwop._inputs_treq_eof(False);
	atomicrwop._inputs_treq_vld(True);
	atomicrwop._inputs_treq_tt(2'b10);
	atomicrwop._inputs_treq_data(0);
	atomicrwop._inputs_treq_crf(False);
	atomicrwop._inputs_treq_prio(2'b01);
	atomicrwop._inputs_treq_ftype(4'b0010);			// Read operation 
	atomicrwop._inputs_treq_dest_id(32'hda34568c);
	atomicrwop._inputs_treq_source_id(32'hda00568c);
	atomicrwop._inputs_treq_addr('d32);		      	// Addresses to the 5th line of memory.hex file 
	atomicrwop._inputs_treq_tid(8'hbf);
	atomicrwop._inputs_treq_ttype(4'b1100);		    	// Atomic increment operation 
	atomicrwop._inputs_treq_byte_count('d8);	   	// Eight bytes corresponds to byte_en  
	atomicrwop._inputs_treq_byte_en(8'b11111111);     	// This feild means the whole 64bit data in memory 
endrule

// The above target request signals are stored in wires to display during simulation 
rule rl_upd(reg_ref_clk == 63);
	$display ("\n \n For 32 bit device - Atomic increment operation" );
	sof<=True;
	eof<=False;
	vld<=True;
	tt<=2'b10;
	data<=0;
	crf<=False;
	prio<=2'b01;
	ftype<=4'b0010;			
	dest_id<=32'hda34568c;
	src_id<=32'hda00568c;
	tid<=8'hbf;
	ttype<=4'b1100;	
	addr<='d32;
	byte_count<='d8;		   
	byte_en<=8'b11111111;	    
endrule
	
// To get the output from RapidIO_AtomicRdWrOp  
rule rl_output;
	wr_outputs_tresp_sof <= atomicrwop.outputs_tresp_sof_ ();
	wr_outputs_tresp_eof <= atomicrwop.outputs_tresp_eof_ (); 
	wr_outputs_tresp_vld <= atomicrwop.outputs_tresp_vld_ ();
	wr_outputs_tresp_tt <= atomicrwop.outputs_tresp_tt_ ();
	wr_outputs_tresp_data <= atomicrwop.outputs_tresp_data_ ();
	wr_outputs_tresp_prio <= atomicrwop.outputs_tresp_prio_ ();
	wr_outputs_tresp_ftype <= atomicrwop.outputs_tresp_ftype_ ();
	wr_outputs_tresp_src_id <= atomicrwop.outputs_tresp_src_id_ ();
	wr_outputs_tresp_dest_id <= atomicrwop.outputs_tresp_dest_id_ ();
	wr_outputs_tresp_status <= atomicrwop.outputs_tresp_status_ ();
	wr_outputs_tresp_tid <= atomicrwop.outputs_tresp_tid_ ();
	wr_outputs_tresp_ttype <= atomicrwop.outputs_tresp_ttype_ ();
endrule

	
// For checking	- displays both the target response and request signal 											     
rule rl_disp;
	if((reg_ref_clk == 3)||(reg_ref_clk == 7)||(reg_ref_clk == 11)||(reg_ref_clk == 15)||(reg_ref_clk == 19)||(reg_ref_clk == 23)||(reg_ref_clk == 27)||(reg_ref_clk == 31)||(reg_ref_clk == 35)||(reg_ref_clk == 39)||(reg_ref_clk == 43)||(reg_ref_clk == 47)||(reg_ref_clk == 51)||(reg_ref_clk == 55)||(reg_ref_clk == 59)||(reg_ref_clk == 63))begin
     $display("\n Target response  				  	 		   Target request \n sof        = %b	                                   sof        = %b\n eof        = %b						         eof        = %b\n vld        = %b						         vld        = %b \n tt         = %b						         tt         = %b \n data       = %h						         data       = %h \n prio       = %b						         prio       = %b \n ftype      = %b						         ftype      = %b           \n source_id  = %b						  source_id  = %b \n dest_id    = %b					  dest_id    = %b \n status     = %b \n tid        = %b						         tid        = %b\n ttype      = %b						         ttype      = %b \n 						         addr       = %b\n							  byte_count = %d\n							  byte_en    = %b", wr_outputs_tresp_sof,sof, wr_outputs_tresp_eof, eof,			wr_outputs_tresp_vld,vld,wr_outputs_tresp_tt,tt, wr_outputs_tresp_data,data, wr_outputs_tresp_prio,prio, wr_outputs_tresp_ftype,ftype, 			               wr_outputs_tresp_src_id,src_id, wr_outputs_tresp_dest_id,dest_id, wr_outputs_tresp_status,wr_outputs_tresp_tid,tid,wr_outputs_tresp_ttype,ttype,addr,byte_count,byte_en);    

	if(wr_outputs_tresp_status=='b0111)
        $display("\n \n         --------------- ERROR --------    \n");
	end
endrule

/* this rule display ony target response signals //

rule rl_disp;
	if((reg_ref_clk == 3)||(reg_ref_clk == 7)||(reg_ref_clk == 11)||(reg_ref_clk == 15)||(reg_ref_clk == 19)||(reg_ref_clk == 23)||(reg_ref_clk == 27)||(reg_ref_clk == 31)||(reg_ref_clk == 35))begin
            $display("\n target response \n sof        = %b \n eof        = %b \n vld        = %b  \n tt         = %b  \n data       = %h  \n prio       = %b  \n ftype      = %b	  \n source_id  = %b \n dest_id    = %b	  \n status     = %b \n tid        = %b	 \n ttype      = %b", wr_outputs_tresp_sof, wr_outputs_tresp_eof,			wr_outputs_tresp_vld,wr_outputs_tresp_tt, wr_outputs_tresp_data, wr_outputs_tresp_prio, wr_outputs_tresp_ftype, 			               wr_outputs_tresp_src_id, wr_outputs_tresp_dest_id, wr_outputs_tresp_status,wr_outputs_tresp_tid,wr_outputs_tresp_ttype);    

	if(wr_outputs_tresp_status=='b0111)
             $display("\n \n         Error    \n");
end
	
endrule
*/

endmodule

endpackage
