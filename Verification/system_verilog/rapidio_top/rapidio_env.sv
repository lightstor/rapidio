/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_ENV_SVH
`define RAPIDIO_ENV_SVH
class rapidio_env extends uvm_env;

  rapidio_ll_agent             rio_ll_agent;
  rapidio_tl_agent                rio_tl_agent;
  rapidio_virtual_sequencer    v_rio_sqncr;
  rapidio_scoreboard 	  sb;
  rapidio_ll_coverage             ll_coverage;
  rapidio_tl_coverage             tl_coverage;

  rapidio_env_config           env_config;
  rapidio_adapter 	    rap_ll_adp;
  rapidio_adapter 	    rap_tl_adp;
  rapidio_reg_block      rap_reg_ll_blk; 
  rapidio_reg_block      rap_reg_tl_blk;

   // Register the predictor ( Actaully this is extends from the uvm_sequencer and sits at the top ) //
    uvm_reg_predictor#(rapidio_ll_sequence_item) ll_predictor; 
    uvm_reg_predictor#(rapidio_tl_sequence_item) tl_predictor; 

  // UVM automation macros  
  `uvm_component_utils_begin(rapidio_env)
   // `uvm_field_object(rapidio_ll_cfg, UVM_ALL_ON);
    `uvm_field_object(env_config, UVM_ALL_ON);
    `uvm_field_object(rio_ll_agent, UVM_ALL_ON)
    `uvm_field_object(rio_tl_agent, UVM_ALL_ON)
    `uvm_field_object(ll_coverage, UVM_ALL_ON)
    `uvm_field_object(tl_coverage, UVM_ALL_ON)
    `uvm_field_object(sb, UVM_ALL_ON)
    `uvm_field_object(ll_predictor, UVM_ALL_ON)
    `uvm_field_object(tl_predictor, UVM_ALL_ON)
    `uvm_field_object(rap_ll_adp, UVM_ALL_ON)
    `uvm_field_object(rap_tl_adp, UVM_ALL_ON)
    `uvm_field_object(rap_reg_ll_blk, UVM_ALL_ON)
    `uvm_field_object(rap_reg_tl_blk, UVM_ALL_ON)

  `uvm_component_utils_end

  // FUNC : Constructor
  function new(string name = "rapidio_env", uvm_component parent = null);
    super.new(name, parent);
  endfunction : new

  // FUNC : Build Phase
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    `uvm_info("RAPIDIO_ENV",$sformatf(" BUILDNG THE  RAPIDIO ENV  \n"),UVM_HIGH); 
    env_config = rapidio_env_config::type_id::create("env_config",this);
     rio_ll_agent = rapidio_ll_agent::type_id::create("rio_ll_agent",this);
     rio_tl_agent = rapidio_tl_agent::type_id::create("rio_tl_agent",this);
     v_rio_sqncr = rapidio_virtual_sequencer::type_id::create("v_rio_sqncr",this);
     rap_ll_adp = rapidio_adapter ::type_id::create("rap_ll_adp",this);
     rap_tl_adp = rapidio_adapter ::type_id::create("rap_tl_adp",this);

      if(!uvm_config_db#(rapidio_env_config)::get(this, "", "rapidio_env_config", env_config))
         `uvm_fatal("AHB_BASE_ENV_FATAL",$sformatf("Cannot get config  from uvm_config_db Have You set it ?"))

    `uvm_info("RAPIDIO_ENV",$sformatf("  RAPIDIO ENV  HAS SCOREBOARD =%d\n",env_config.has_scoreboard),UVM_HIGH); 
     if(!uvm_config_db#(rapidio_reg_block)::get(null,"","FROM_BASETEST2SEQ",rap_reg_ll_blk))
       `uvm_fatal("RAPIDIO_RAL_ENV",$sformatf(" NO THE CONFIGURATION HAS BEEN SET \n"))
      else
       `uvm_info("RAPIDIO_RAL_ENV",$sformatf(" YES THE CONFIGURATION HAS BEEN SET \n"),UVM_HIGH)

     if (env_config.has_scoreboard == 1)
        sb = rapidio_scoreboard::type_id::create("sb",this);
     else
       `uvm_info("RAPIDIO_ENV",$sformatf("  RAPIDIO ENV NO SCOREBOARD \n"),UVM_HIGH); 
     
     if(env_config.has_ral== 1)
      begin
       ll_predictor = uvm_reg_predictor #(rapidio_ll_sequence_item)::type_id::create("ll_predictor",this); 
       tl_predictor = uvm_reg_predictor #(rapidio_tl_sequence_item)::type_id::create("tl_predictor",this); 
       `uvm_info("RAPIDIO_RAL_ENV",$sformatf(" YES THE PREDICTOR HAS BEEN CERATED \n"),UVM_HIGH)
      end
     else
       `uvm_info("RAPIDIO_RAL_ENV",$sformatf(" N0 THE PREDICTOR was CERATED \n"),UVM_HIGH)

        if (env_config.has_coverage == 1)
	begin
         `uvm_info("RAPIDIO_ENV",$sformatf("RAPIDIO ENV  HAS COVERAGE =%d\n",env_config.has_coverage),UVM_HIGH); 
         ll_coverage = rapidio_ll_coverage::type_id::create("ll_coverage",this);
         tl_coverage = rapidio_tl_coverage::type_id::create("tl_coverage",this);
          end
              else
      `uvm_info("RAPIDIO_ENV",$sformatf(" RAPIDIO ENV NO COVERAGE \n"),UVM_HIGH); 
 
    void'(uvm_config_db#(rapidio_ll_agent_config)::set(this,"rapidio_agent*","rapidio_ll_agent_config",env_config.ll_cfg));
    void'(uvm_config_db#(rapidio_tl_agent_config)::set(this,"rapidio_agent*","rapidio_tl_agent_config",env_config.tl_cfg));
    rio_ll_agent.is_initiator = env_config.ll_cfg.is_initiator;
    
  endfunction : build_phase

 function void connect_phase(uvm_phase phase);
      v_rio_sqncr.rio_ll_seqcr = rio_ll_agent.rapidio_ll_seqr;
      v_rio_sqncr.rio_tl_seqcr = rio_tl_agent.rapidio_tl_seqr;

     // RAL MODEL BEGINS
      //if(env_congif.rapidio_agent_congif.active == UVM_ACTIVE)
      if(env_config.is_active == UVM_ACTIVE)
       begin
        env_config.rap_reg_ll_blk.RIO_MAP.set_sequencer(rio_ll_agent.rapidio_ll_seqr,rio_ll_agent.rap_ll_adp);
        env_config.rap_reg_tl_blk.RIO_MAP.set_sequencer(rio_tl_agent.rapidio_tl_seqr,rio_tl_agent.rap_tl_adp);
       ll_predictor.map = env_config.rap_reg_ll_blk.RIO_MAP;
       tl_predictor.map = env_config.rap_reg_tl_blk.RIO_MAP;
                    
       ll_predictor.adapter = rap_ll_adp;
       tl_predictor.adapter = rap_tl_adp;
       rio_ll_agent.rapidio_ll_monitor.mon_ll_ral_port.connect(ll_predictor.bus_in); 
       rio_tl_agent.rapidio_tl_mon.mon_tl_ral_port.connect(tl_predictor.bus_in);    
      end
       
      // RAL MODEL ENDS // 
 

       if(env_config.has_scoreboard) begin
        rio_ll_agent.rapidio_ll_bfm.dr2sb_ll_port.connect(sb.dr2sb_ll_export);
        rio_tl_agent.rapidio_tl_drvr.dr2sb_tl_port.connect(sb.dr2sb_tl_export);
        rio_ll_agent.rapidio_ll_monitor.mon2sb_ll_port.connect(sb.mon2sb_ll_export);
        rio_tl_agent.rapidio_tl_mon.mon2sb_tl_port.connect(sb.mon2sb_tl_export);
        `uvm_info("RAPIDIO_ENV",$sformatf(" SCOREBOARD HAS BEEN CONNECTED =%d \n",env_config.has_scoreboard),UVM_HIGH); 
       end
 
   //COVERAGE PORT CONNECTIONS
   if(env_config.has_coverage)
  begin
     rio_ll_agent.rapidio_ll_bfm.dr2cov_ll_port.connect(ll_coverage.dr2cov_ll_export);
     rio_tl_agent.rapidio_tl_drvr.dr2cov_tl_port.connect(tl_coverage.dr2cov_tl_export);
     rio_ll_agent.rapidio_ll_monitor.mon2cov_ll_port.connect(ll_coverage.mon2cov_ll_export);
     rio_tl_agent.rapidio_tl_mon.mon2cov_tl_port.connect(tl_coverage.mon2cov_tl_export);
    `uvm_info("RAPIDIO_ENV",$sformatf(" COVERAGE HAS BEEN CONNECTED =%d \n",env_config.has_scoreboard),UVM_HIGH); 

  end


     rio_ll_agent.rapidio_ll_bfm.put_pkt_port.connect(rio_tl_agent.ingress_ll_to_tl_pkt_fifo.put_export);
      rio_ll_agent.rapidio_ll_bfm.put_cntrl_port.connect(rio_tl_agent.ingress_ll_to_tl_cntrl_fifo.put_export);
      rio_tl_agent.rapidio_tl_mon.put_ll_pkt_port.connect(rio_ll_agent.ingress_ll_pkt_fifo.put_export);
      rio_tl_agent.rapidio_tl_mon.put_ll_cntrl_port.connect(rio_ll_agent.ingress_ll_cntrl_fifo.put_export);
 endfunction:connect_phase


endclass : rapidio_env

`endif
