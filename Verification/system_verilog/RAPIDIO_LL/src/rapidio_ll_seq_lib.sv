/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_MASTER_SEQ_LIB_SVH
`define RAPIDIO_MASTER_SEQ_LIB_SVH

// SEQUENCE: rapidio_initiator_base_sequence - defining a base sequence that hides the common tasks
virtual class rapidio_ll_base_sequence extends uvm_sequence #(rapidio_ll_sequence_item);

  // Temp variable declaration


  // sequencer & sequence_item handle for peek
  rapidio_ll_sequencer               rapidio_ll_seqr;
  rapidio_ll_sequence_item           pkt_transfer;

  // FUNC : Constructor
  function new(string name="rapidio_ll_base_sequence");
    super.new(name);
  endfunction :new
   
  // TASK : body
  virtual task body();
    if(!$cast(rapidio_ll_seqr, m_sequencer))
      `uvm_error("RAPIDIO_LL_BASE_SEQ_ERR",$sformatf("CASTING FAILED"))

    // Body
  endtask : body

endclass : rapidio_ll_base_sequence


// SEQUENCE: rapidio_initiator_write_seq  - Basic WRITE sequence for 1 transaction
class rapidio_ll_nwrite_seq extends rapidio_ll_base_sequence;

    // Instantiate the rapidio register block //
    rand uvm_reg_data_t set_data;
    uvm_status_e status;

  // FUNC : Constructor
  function new(string name="rapidio_ll_nwrite_seq");
    super.new(name);
  endfunction :new

  // UVM automation macros   
  `uvm_object_utils_begin(rapidio_ll_nwrite_seq)
  `uvm_object_utils_end

  // TASK : body                          
  virtual task body();
    begin 
     rapidio_reg_block rap_reg_ll_blk;
     if(!uvm_config_db#(rapidio_reg_block)::get(null,"","FROM_BASETEST2SEQ",rap_reg_ll_blk))
     `uvm_fatal("RAPIDIO_SEQ_LIB",$sformatf(" YES THE RAL CONFIGURATION HAS NOT SET \n"))
    else
     begin
     `uvm_info("RAPIDIO_SEQ_LIB",$sformatf(" YES THE RAL CONFIGURATION HAS SET \n"),UVM_HIGH)
     end
       if(rap_reg_ll_blk == null)
        $display(" The object has not been created \n");
       else
        $display(" yes the object was created \n");

        $display(" writing to the DUT register has started \n");
       rap_reg_ll_blk.dev_iden_car.set(32'h100);
       rap_reg_ll_blk.dev_iden_car.set(32'h200); 
       rap_reg_ll_blk.dev_info_car.set(32'd15); 
       rap_reg_ll_blk.src_oprt_car.set(32'd45);  
       rap_reg_ll_blk.ass_info_car.set(32'd22); 
       rap_reg_ll_blk.ass_info_car.set(32'd244); 
       rap_reg_ll_blk.psll_csr.set(32'd456); 
       rap_reg_ll_blk.lcsba0_csr.set(32'd55);
       rap_reg_ll_blk.dest_oprt_car.set(32'd23);  
       rap_reg_ll_blk.prs_ele_ftr.set(32'd18);  
       rap_reg_ll_blk.swth_prt_info.set(32'd33); 
       rap_reg_ll_blk.lcsba1_csr.set(32'd786);
  end
 
     `uvm_do_with (pkt_transfer, {ftype == 4'd5;ttype== 4'd4 ;data.size <= wr_dword_count ; data.size != 0; });  
  endtask :body
endclass :rapidio_ll_nwrite_seq 


// SEQUENCE: rapidio_initiator_write_seq  - Basic WRITE sequence for 1 transaction
class rapidio_ll_nwrite_r_seq extends rapidio_ll_base_sequence;

    // Instantiate the rapidio register block //
    rand uvm_reg_data_t set_data;
    uvm_status_e status;

  // FUNC : Constructor
  function new(string name="rapidio_ll_nwrite_r_seq");
    super.new(name);
  endfunction :new

  // UVM automation macros   
  `uvm_object_utils_begin(rapidio_ll_nwrite_r_seq)
  `uvm_object_utils_end

  // TASK : body                          
  virtual task body();
    begin 
     rapidio_reg_block rap_reg_ll_blk;
     if(!uvm_config_db#(rapidio_reg_block)::get(null,"","FROM_BASETEST2SEQ",rap_reg_ll_blk))
     `uvm_fatal("RAPIDIO_SEQ_LIB",$sformatf(" YES THE RAL CONFIGURATION HAS NOT SET \n"))
    else
     begin
     `uvm_info("RAPIDIO_SEQ_LIB",$sformatf(" YES THE RAL CONFIGURATION HAS SET \n"),UVM_HIGH)
     end
       if(rap_reg_ll_blk == null)
        $display(" The object has not been created \n");
       else
        $display(" yes the object was created \n");

        $display(" writing to the DUT register has started \n");
       rap_reg_ll_blk.dev_iden_car.set(32'h100);
       rap_reg_ll_blk.dev_iden_car.set(32'h200); 
       rap_reg_ll_blk.dev_info_car.set(32'd15); 
       rap_reg_ll_blk.src_oprt_car.set(32'd45);  
       rap_reg_ll_blk.ass_info_car.set(32'd22); 
       rap_reg_ll_blk.ass_info_car.set(32'd244); 
       rap_reg_ll_blk.psll_csr.set(32'd456); 
       rap_reg_ll_blk.lcsba0_csr.set(32'd55);
       rap_reg_ll_blk.dest_oprt_car.set(32'd23);  
       rap_reg_ll_blk.prs_ele_ftr.set(32'd18);  
       rap_reg_ll_blk.swth_prt_info.set(32'd33); 
       rap_reg_ll_blk.lcsba1_csr.set(32'd786);
  end
 
     `uvm_do_with (pkt_transfer, {ftype == 4'd5;ttype== 4'd5 ;data.size <= wr_dword_count ; data.size != 0; });  
  endtask :body
endclass :rapidio_ll_nwrite_r_seq 




// SEQUENCE: rapidio_initiator_read_seq - Basic READ & PEEKING PRDATA sequence for 1 transaction
class rapidio_ll_nread_seq extends rapidio_ll_base_sequence;

   uvm_reg_data_t read_data;
   uvm_status_e status;

  // FUNC : Constructor
  function new(string name="rapidio_ll_nread_seq");
    super.new(name);
  endfunction :new
 
  // UVM automation macros
  `uvm_object_utils_begin(rapidio_ll_nread_seq)
  `uvm_object_utils_end

  // TASK : body                          
  virtual task body();
    begin

     int unsigned src_oprt,dest_oprt,pres_ele,swth_prt,dev_iden,dev_info,ass_iden,ass_info,psll,lcsba0,lcsba1;
     rapidio_reg_block rap_reg_ll_blk;

     if(!uvm_config_db#(rapidio_reg_block)::get(null,"","FROM_BASETEST2SEQ",rap_reg_ll_blk))
     `uvm_fatal("RAPIDIO_SEQ_LIB",$sformatf(" RAL CONFIGURATION HAS NOT SET \n"))
    else
     begin
       `uvm_info("RAPIDIO_SEQ_LIB",$sformatf(" RAL CONFIGURATION HAS SET \n"),UVM_HIGH)
        src_oprt  = rap_reg_ll_blk.src_oprt_car.get();  
        dest_oprt = rap_reg_ll_blk.dest_oprt_car.get();  
        pres_ele  = rap_reg_ll_blk.prs_ele_ftr.get();  
        swth_prt  = rap_reg_ll_blk.swth_prt_info.get(); 
        dev_iden  = rap_reg_ll_blk.dev_iden_car.get(); 
        dev_info  = rap_reg_ll_blk.dev_info_car.get(); 
        ass_iden  = rap_reg_ll_blk.ass_iden_car.get(); 
        ass_info  = rap_reg_ll_blk.ass_info_car.get();
        psll      = rap_reg_ll_blk.psll_csr.get(); 
        lcsba0    = rap_reg_ll_blk.lcsba0_csr.get(); 
        lcsba1    = rap_reg_ll_blk.lcsba1_csr.get(); 
      end
  end
     `uvm_do_with (pkt_transfer, {ftype == 4'd2;ttype== 4'd4; });  
  endtask :body

endclass :rapidio_ll_nread_seq 

class rapidio_ll_response_seq extends rapidio_ll_base_sequence;

  rapidio_ll_sequence_item  pkt_from_mon; 

// FUNC : Constructor
  function new(string name="rapidio_ll_response_seq");
    super.new(name);
    pkt_from_mon = rapidio_ll_sequence_item::type_id::create("pkt_from_mon");
  endfunction

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_ll_response_seq)
  `uvm_object_utils_end

  // TASK : body                          
  virtual task body();
   forever begin 
     $cast(rapidio_ll_seqr, m_sequencer); 
      rapidio_ll_seqr.response_ph_port.peek(pkt_from_mon);
     //`uvm_info("RAPIDIO_LL_SEQ_INFO",$sformatf("PEEK PKT =%0d\n",pkt_from_mon.rd_dword_count), UVM_LOW);

      if ((pkt_from_mon.ftype == 2) && (pkt_from_mon.ttype == 4)) //NREAD
      begin 
        `uvm_do_with (pkt_transfer, {ftype == 4'd13;ttype== 4'd8  ; status == 4'd0 ;data.size == rd_dword_count ; });
         break;
      end

      if ((pkt_from_mon.ftype == 5) && (pkt_from_mon.ttype == 5)) //NWRITE_R 
        `uvm_do_with (pkt_transfer, {ftype == 4'd13;ttype== 4'd0 | ttype== 4'd8  ; status == 4'd0| status == 4'd7 ; }); 
         break;
    end
  endtask :body

endclass :rapidio_ll_response_seq 



class rapidio_ll_err_response_seq extends rapidio_ll_base_sequence;

  rapidio_ll_sequence_item  pkt_from_mon; 

// FUNC : Constructor
  function new(string name="rapidio_ll_err_response_seq");
    super.new(name);
    pkt_from_mon = rapidio_ll_sequence_item::type_id::create("pkt_from_mon");
  endfunction

 
  `uvm_object_utils_begin(rapidio_ll_err_response_seq)
  `uvm_object_utils_end

  // TASK : body                          
  virtual task body();
   forever begin 
     $cast(rapidio_ll_seqr, m_sequencer); 
      rapidio_ll_seqr.response_ph_port.peek(pkt_from_mon);
      if ((pkt_from_mon.ftype == 2) && (pkt_from_mon.ttype == 4)) //NREAD
      begin 
        `uvm_do_with (pkt_transfer, {ftype == 4'd13;ttype== 4'd0  ;  status == 4'd7 ; });
         break;
      end

      if ((pkt_from_mon.ftype == 5) && (pkt_from_mon.ttype == 5)) //NWRITE_R 
        `uvm_do_with (pkt_transfer, {ftype == 4'd13;ttype== 4'd0   ; status == 4'd0  | status == 4'd7 ; });
         break; 
    end
  endtask :body

endclass :rapidio_ll_err_response_seq 


`endif
