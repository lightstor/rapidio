/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_LL_ENV_CONFIG_SVH
`define RAPIDIO_LL_ENV_CONFIG_SVH

// CLASS: rapidio_env_config
class rapidio_ll_env_config extends uvm_object;

  // Variable declarations
  string                      env_name         = "DEFAULT";   
  int                         num_slaves       = 1;
  bit                         has_scoreboard   = 1;

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_ll_env_config)
    `uvm_field_string(env_name, UVM_ALL_ON)
    `uvm_field_int(num_slaves, UVM_ALL_ON)
    `uvm_field_int(has_scoreboard, UVM_ALL_ON)
  `uvm_object_utils_end

  // FUNC : Constructor
  function new(string name = "rapidio_ll_env_config");
    super.new(name);
  endfunction : new

endclass: rapidio_ll_env_config

`endif
