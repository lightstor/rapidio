
USES_DPI = 1

ifeq ($(TEST),nread)
 UVM_TESTNAME =test_b2b_nread
endif

ifeq ($(TEST),nread_err_resp)
 UVM_TESTNAME =test_b2b_nread_err_resp
endif

ifeq ($(TEST),nwrite)
 UVM_TESTNAME =test_b2b_nwrite
endif

ifeq ($(TEST),nwrite_resp)
 UVM_TESTNAME =test_b2b_nwrite_resp
endif

#ifeq ($(TEST),wr_burst_m)
# UVM_TESTNAME =test_1m_1s_burst_read_write_memory
#endif
#
#ifeq ($(TEST),rd_rom)
# UVM_TESTNAME =test_1m_1s_read_write_rom
#endif
#
#ifeq ($(TEST),wr_fifo)
# UVM_TESTNAME =test_1m_1s_read_write_fifo
#endif
#
#ifeq ($(TEST),wr_burst_fifo)
# UVM_TESTNAME =test_1m_1s_burst_read_write_fifo
#endif
#
#ifeq ($(TEST),wr_cons_m)
# UVM_TESTNAME =test_1m_1s_burst_consecutive_read_write_memory
#endif
#
#ifeq ($(TEST),wr_ral_reg)
# UVM_TESTNAME =test_1m_1s_read_write_ral_reg
#endif
#
#ifeq ($(TEST),error)
# UVM_TESTNAME =test_1m_1s_read_write_memory_error
#endif
#
#ifeq ($(TEST),wr_user_cfg_m)
# UVM_TESTNAME =test_1m_1s_user_config_read_write_memory
#endif
#
#ifeq ($(TEST),reset)
# UVM_TESTNAME =test_1m_1s_reset
#endif

######
USES_DPI = 1
ifdef UVM_NO_DPI
  USES_DPI=0
endif

#---------------------------------------------------------------
# Define Variables
#---------------------------------------------------------------

UVM_HOME = /apps/mentor/10.1b/questasim/verilog_src/uvm-1.1a

LIBDIR  = $(HOME)/lib
LOGS    = /logs
#GCC     = $(MTI_HOME)/gcc-4.1.2-linux/bin/g++
GCC     = gcc
TEST    = /usr/bin/test
BITS    = 32
LIBNAME = uvm_dpi
DPI_SRC = /apps/mentor/10.1b/questasim/verilog_src/uvm-1.1a/src/dpi/uvm_dpi.cc
VSIM_OPT = -novopt -classdebug -msgmode both -uvmcontrol=all -OVMdebug -sv_lib /apps/mentor/10.1b/questasim/uvm-1.1a/linux_x86_64/uvm_dpi -assertdebug -assertcover


GCCCMD =  $(GCC) \
        -m$(BITS) \
        -fPIC \
        -DQUESTA \
        -g \
        -W \
        -shared \
        -x c \
        -I$(MTI_HOME)/include \
        $(DPI_SRC) \
        -o $(LIBDIR)/$(LIBNAME).so

GCC_WINCMD = \
        $(WIN_GCC) \
        -g \
        -DQUESTA \
        -W \
        -shared \
        -Bsymbolic \
        -x c \
        -I$(MTI_HOME)/include \
        $(DPI_SRC) \
        -o $(LIBDIR)/$(LIBNAME).dll \
		$(MTI_HOME)/win32/mtipli.dll -lregex

WIN_GCC = $(MTI_HOME)/gcc-4.2.1-mingw32vc9/bin/g++.exe

VLIB =  vlib work

VLOG =  vlog \
        -timescale "1ns/1ns" \
        $(DPILIB_VLOG_OPT) \
        $(VLOG_OPT) \
        $(OPT_C) \
        -mfcu \
        -suppress 2181 \
        +acc=rmb \
        -writetoplevels questa.tops \
	-L mtiUvm +cover \
        +define+DUMMY_DUT \
	+incdir+/apps/mentor/10.1b/questasim/verilog_src/uvm-1.1a/src \
	+incdir+/apps/mentor/10.1b/questasim/verilog_src/questa_uvm_pkg-1.2/src/ \
	/apps/mentor/10.1b/questasim/verilog_src/questa_uvm_pkg-1.2/src/questa_uvm_pkg.sv


VSIM =  vsim \
        $(DPILIB_VSIM_OPT) \
        $(VSIM_OPT) \
        $(OPT_R) \
        -c \
        -do vsim.do \
        -l $(UVM_TESTNAME).questa.log \
        -f questa.tops  

N_ERRS = 0
N_FATALS = 0

CHECK = \
	@$(TEST) \( `grep -c 'UVM_ERROR :    $(N_ERRS)' questa.log` -eq 1 \) -a \
		 \( `grep -c 'UVM_FATAL :    $(N_FATALS)' questa.log` -eq 1 \)

#---------------------------------------------------------------
# If USES_DPI is set, enables compilation and loading of DPI
# libraries. Enabling DPI adds +acc on command line, which
# may adversely affect simulator performance.
#---------------------------------------------------------------

ifeq ($(USES_DPI),1)
  DPILIB_VLOG_OPT = 
  DPILIB_VSIM_OPT = -sv_lib $(LIBDIR)/uvm_dpi 
  DPILIB_TARGET = dpi_lib$(BITS)
else
  DPILIB_VLOG_OPT = +define+UVM_NO_DPI 
  DPILIB_VSIM_OPT = 
  DPILIB_TARGET =
endif
  

#---------------------------------------------------------------
# Define Targets
#
# vlog and vsim targets defined in individual examples
#---------------------------------------------------------------


help: 
	@echo "Usage:  make -f Makefile_B2B.questa [target(s)]"
	@echo ""
	@echo "Typical: make -f Makefile_B2B.questa all"
	@echo ""
	@echo "where target is any of"
	@echo ""
	@echo "  dpi_lib     - compile DPI lib (use BITS=XX, def=32)"
	@echo "  dpi_lib32   - compile DPI lib for 32-bit Linux (BITS=32)"
	@echo "  dpi_lib64   - compile DPI lib for 64-bit Linux (BITS=64)"
	@echo "  dpi_libWin  - compile DPI lib for Windows"
	@echo ""
	@echo "  clean       - removes all derived files"
	@echo "  vlib        - creates work library"
	@echo "  prepare     - invokes clean, vlib, and dpi_lib targets"
	@echo ""
	@echo "When this makefile is included by a Makefile from an example"
	@echo "sub-directory, additional targets should be available:"
	@echo ""
	@echo "  all       - invokes targets prepare, vlog, and vsim"
	@echo "  vlog      - invokes the vlog compiler"
	@echo "  vsim      - invokes the vsim simulator"
	@echo ""
	@echo "Variables: specify any of the following on the make command line"
	@echo ""
	@echo "  UVM_HOME  - root directory of the UVM library (default:..)"
	@echo "  UVM_VERBOSITY - verbosity level for vsim (default:UVM_MEDIUM)"
	@echo "  BITS      - the bus architecture: 32 or 64 (default:32)"
	@echo "  LIBNAME   - the root name of the dpi library (default:uvm_dpi)"
	@echo "  LIBDIR    - the location to put the dpi lib (default:UVM_HOME/lib)"
	@echo ""

prepare: clean vlib $(DPILIB_TARGET)


dpi_lib:
	mkdir -p $(LIBDIR)
	$(GCCCMD)

dpi_libWin:
	mkdir -p $(LIBDIR)
	$(GCC_WINCMD)

dpi_lib32:
	make -f Makefile_B2B.questa BITS=32 dpi_lib

dpi_lib64:
	make -f Makefile_B2B.questa LIBNAME=uvm_dpi BITS=64 dpi_lib

vlib: $(DPILIB_TARGET)
	vlib work

clean:
	rm -rf *~ work vsim.wlf* *.log questa.tops transcript *.vstf cov/ coverage.ucdb coverage.txt 

all: run

comp: vlib
	$(VLOG) +incdir+../RAPIDIO_LL/src \
            +incdir+../RAPIDIO_TL/src \
            +incdir+../rapidio_top \
            +incdir+../top \
   	../top/rapidio_b2b_top.sv 

run: clean comp
	$(VSIM) +UVM_TESTNAME=$(UVM_TESTNAME) +UVM_VERBOSITY=UVM_HIGH -solvefaildebug -sv_seed random 
	vcover report -html -htmldir cov coverage.ucdb

alt:
	qverilog \
        -timescale "1ns/1ns" \
        +acc=rmb \
        +incdir+/apps/mentor/10.1b/questasim/verilog_src/uvm-1.1a/src+../src \
        /apps/mentor/10.1b/questasim/verilog_src/uvm-1.1a/src/uvm.sv \
        test.sv \
        $(DPI_SRC) \
        -R \
        +UVM_TESTNAME=test_1m_1s_random_read_write \
        -gui \
        -do "coverage save -onexit coverage.ucdb; run -all; q" \
        -l questa.log 

