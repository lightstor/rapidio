package RapidIO_Cache_Coherence_L2ControlInterface;

interface Ifc_L2_CacheContrl;

method Action _inputs_packet_L2Contrl(Packet pkt);
method Action _inputs_Processor_Agent_Is_Ready(Bool value);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt);
method Action _inputs_next_request();
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt); 
method Action _load_From_Memory_And_Change_State(Physical_Address_Type physical_Address,State s);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(Packet pkt);
method Action _inputs_From_Processor_Agent_To_L2contrl_alterState(Packet pkt);
method Packet _get_data_L2Contrl(Packet pkt);
method Packet _outputs_Packet_L2Contrl();
method State  _gets_status_block(Physical_Address_Type physical_Address);


method Packet _outputs_data_mkTb(); 
method Bool _outputs_AlterCalled();
//method Packet _outputs_Request_To_Processor_Agent();
method Action _inputs_packet_To_L2Contrl_From_processor_Agent_Resp(Packet pkt);
 // Local Response

method Action _inputs_packet_L2Contrl_From_RapidIO_Req(Packet pkt); // At Remote.
method Packet _outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // At Remote
method ActionValue#(Packet) _outputs_Request_To_Processor_Agent();


endinterface


endpackage
