/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Physical Layer CRC-16 Generation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1. It is used to generate the CRC-16 generation.
-- 2. Design is implemented as mentioned in the RapidIO specification 3.0. 
-- 3. The Control and Data Packet signals from the Link Transmit interface is used as input signals.
-- 4. Using Tx_Rem Signal, the last byte of the packet is determined.
-- 5. During the first packet of the transaction, Old Check Symbol value is assigned to 16'hffff. 
-- 6. Function is developed to perform the CRC-16 Calculation (as specified in the RapidIO specification).
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

// To Follow the Endianness, the Bits are modified.
// 1. RapidIO Supports Big Endian and Bluespec Supports Little Endian. 
// So Equation Table is modified to support Little Endian. 


package RapidIO_PhyCRC16Generation;

import RapidIO_DTypes ::*;

// Function is used to perform three level operation of CRC-16.
function Bit#(16) fn_CRC16Generation (Bit#(16) old_check_symbol, Bit#(16) data_in);

// 1st Level Operation (XORing Data and Old Symbol)
    Bit#(16) lv_IntermediateSymbol = data_in ^ old_check_symbol; 

// 2nd Level Operation (Equation Network)
    Bit#(1) lv_Check_Symbol_15 = lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[0];
    Bit#(1) lv_Check_Symbol_14 = lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];
    Bit#(1) lv_Check_Symbol_13 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];
    Bit#(1) lv_Check_Symbol_12 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];
    Bit#(1) lv_Check_Symbol_11 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4];
    Bit#(1) lv_Check_Symbol_10 = lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[1];  
    Bit#(1) lv_Check_Symbol_9 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];
    Bit#(1) lv_Check_Symbol_8 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];
    Bit#(1) lv_Check_Symbol_7 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];
    Bit#(1) lv_Check_Symbol_6 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4];
    Bit#(1) lv_Check_Symbol_5 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[5];
    Bit#(1) lv_Check_Symbol_4 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[6];
    Bit#(1) lv_Check_Symbol_3 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[0];
    Bit#(1) lv_Check_Symbol_2 = lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];
    Bit#(1) lv_Check_Symbol_1 = lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];
    Bit#(1) lv_Check_Symbol_0 = lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];

// 3rd Level Operation - Generates Next Check Symbol 
    return {lv_Check_Symbol_15, lv_Check_Symbol_14, lv_Check_Symbol_13, lv_Check_Symbol_12, lv_Check_Symbol_11, lv_Check_Symbol_10, lv_Check_Symbol_9, lv_Check_Symbol_8, lv_Check_Symbol_7, lv_Check_Symbol_6, lv_Check_Symbol_5, lv_Check_Symbol_4, lv_Check_Symbol_3, lv_Check_Symbol_2, lv_Check_Symbol_1, lv_Check_Symbol_0};

endfunction 


interface Ifc_RapidIO_PhyCRC16Generation;
 //-- Input Signals - Control and Data Packet
 method Action _link_tx_sof_n (Bool value);
 method Action _link_tx_eof_n (Bool value);
 method Action _link_tx_vld_n (Bool value);
// method Bool link_tx_rdy_n_ ();
 method Action _link_tx_data (DataPkt value);
 method Action _link_tx_rem (Bit#(4) value);
 method Action _link_tx_crf (Bool value);

 // -- Output Signal - CRC-16 
 method Bit#(16) outputs_CRC16_ ();

endinterface : Ifc_RapidIO_PhyCRC16Generation


(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_PhyCRC16Generation (Ifc_RapidIO_PhyCRC16Generation);
 //-- Input Signals as Wires
Wire#(Bool) wr_tx_sof <- mkDWire (True);
Wire#(Bool) wr_tx_eof <- mkDWire (True);
Wire#(Bool) wr_tx_vld <- mkDWire (True);
// method Bool link_tx_rdy_n_ ();
Wire#(DataPkt) wr_tx_data <- mkDWire (0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire (0);
Wire#(Bool) wr_tx_crf <- mkDWire (True);

 // -- Output Signal 
Wire#(Bit#(16)) wr_CRC16 <- mkDWire (0);

// Internal Wires and Registers
Wire#(Bit#(16)) wr_CheckSymbolGen_0 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolGen_1 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolGen_2 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolGen_3 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolGen_4 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolGen_5 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolGen_6 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolGen_7 <- mkDWire (0);
Reg#(Bit#(16)) rg_OldCheckSymbol <- mkReg (0);

// Rules -- 
/*
-- CRC value is held in a wire for every stage.
-- Final Stage of the CRC value is stored in  a Register. 
*/
rule rl_CRC_16_Generation (wr_tx_vld == False);
    Bit#(16) lv_OldCheckSymbol = (wr_tx_sof == False) ? 16'hffff : rg_OldCheckSymbol;
    Bit#(16) lv_InitialData = (wr_tx_sof == False) ? {6'h00, wr_tx_data[121:112]} : wr_tx_data[127:112]; // To set 6 bit ACK ID bits to 0. 
    Bit#(16) lv_CheckSymbolGen_0 = fn_CRC16Generation (lv_OldCheckSymbol, lv_InitialData);
    wr_CheckSymbolGen_0 <= fn_CRC16Generation (lv_OldCheckSymbol, wr_tx_data[127:112]);

    Bit#(16) lv_CheckSymbolGen_1 = fn_CRC16Generation (lv_CheckSymbolGen_0, wr_tx_data[111:96]);
    wr_CheckSymbolGen_1 <= fn_CRC16Generation (lv_CheckSymbolGen_0, wr_tx_data[111:96]);

    Bit#(16) lv_CheckSymbolGen_2 = fn_CRC16Generation (lv_CheckSymbolGen_1, wr_tx_data[95:80]);
    wr_CheckSymbolGen_2 <= fn_CRC16Generation (lv_CheckSymbolGen_1, wr_tx_data[95:80]);

    Bit#(16) lv_CheckSymbolGen_3 = fn_CRC16Generation (lv_CheckSymbolGen_2, wr_tx_data[79:64]);
    wr_CheckSymbolGen_3 <= fn_CRC16Generation (lv_CheckSymbolGen_2, wr_tx_data[79:64]);

    Bit#(16) lv_CheckSymbolGen_4 = fn_CRC16Generation (lv_CheckSymbolGen_3, wr_tx_data[63:48]);
    wr_CheckSymbolGen_4 <= fn_CRC16Generation (lv_CheckSymbolGen_3, wr_tx_data[63:48]);

    Bit#(16) lv_CheckSymbolGen_5 = fn_CRC16Generation (lv_CheckSymbolGen_4, wr_tx_data[47:32]);
    wr_CheckSymbolGen_5 <= fn_CRC16Generation (lv_CheckSymbolGen_4, wr_tx_data[47:32]);

    Bit#(16) lv_CheckSymbolGen_6 = fn_CRC16Generation (lv_CheckSymbolGen_5, wr_tx_data[31:16]);
    wr_CheckSymbolGen_6 <= fn_CRC16Generation (lv_CheckSymbolGen_5, wr_tx_data[31:16]);
    
    Bit#(16) lv_CheckSymbolGen_7 = fn_CRC16Generation (lv_CheckSymbolGen_6, wr_tx_data[15:0]);
    wr_CheckSymbolGen_7 <= fn_CRC16Generation (lv_CheckSymbolGen_6, wr_tx_data[15:0]);
    rg_OldCheckSymbol <= fn_CRC16Generation (lv_CheckSymbolGen_6, wr_tx_data[15:0]);
endrule 

 // Input and Output Methods Definitions 
 method Action _link_tx_sof_n (Bool value);
    wr_tx_sof <= value;
 endmethod 
 method Action _link_tx_eof_n (Bool value);
    wr_tx_eof <= value;
 endmethod 
 method Action _link_tx_vld_n (Bool value);
    wr_tx_vld <= value; 
 endmethod 
// method Bool link_tx_rdy_n_ ();
 method Action _link_tx_data (DataPkt value);
    wr_tx_data <= value;
 endmethod 
 method Action _link_tx_rem (Bit#(4) value);
    wr_tx_rem <= value; 
 endmethod 
 method Action _link_tx_crf (Bool value);
    wr_tx_crf <= value; 
 endmethod 

 // -- Output Signal - CRC-16 
 method Bit#(16) outputs_CRC16_ ();
    if (wr_tx_eof == False) begin
        if (wr_tx_rem[3:1] == 3'b000)
            return wr_CheckSymbolGen_0;
        else if (wr_tx_rem[3:1] == 3'b001)
            return wr_CheckSymbolGen_1;
        else if (wr_tx_rem[3:1] == 3'b010)
            return wr_CheckSymbolGen_2;
        else if (wr_tx_rem[3:1] == 3'b011)
            return wr_CheckSymbolGen_3;
        else if (wr_tx_rem[3:1] == 3'b100)
            return wr_CheckSymbolGen_4;
        else if (wr_tx_rem[3:1] == 3'b101)
            return wr_CheckSymbolGen_5;
        else if (wr_tx_rem[3:1] == 3'b110)
            return wr_CheckSymbolGen_6;
        else 
            return wr_CheckSymbolGen_7;
    end 
    else 
        return 0; 
 endmethod 

endmodule : mkRapidIO_PhyCRC16Generation

endpackage : RapidIO_PhyCRC16Generation
