/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Top Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1. Connects Main Core and Register Manager modules of the RapidIO      
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_TopModule;

/*
-- Importing the RapidIO Main Core and Register Manager modules 
*/
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_MainCore ::*;
import RapidIO_RegisterManager ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitiatorRespIFC ::*;
import RapidIO_TargetReqIFC ::*;
import RapidIO_MaintenanceReqIFC ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import RapidIO_PktTransportParse ::*;
import RapidIO_RxPktFTypeAnalyse ::*;

/*
-- Initiator Request, Initiator Response, Target Request and Target Response signals are 
-- taken as output. 
-- Maintenance Request and Response signals are connected directly to the Register Manager
*/ 
interface Ifc_RapidIO_TopModule;
 interface Ifc_InitiatorReqIFC _InitReqInterface;
 interface Ifc_TargetRespIFC _TargetRespInterface;

 interface Ifc_InitiatorRespIFC _InitRespInterface;
 interface Ifc_TargetReqIFC _TargetReqInterface;

 interface Ifc_LinkInterfaceTx _TxPktsLinkIfc;
 interface Ifc_LinkInterfaceRx _RxPktsLinkIfc;

endinterface : Ifc_RapidIO_TopModule


(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_TopModule (Ifc_RapidIO_TopModule);

// Module Instantiation 
Ifc_RapidIO_MainCore rio_MainCore <- mkRapidIO_MainCore ();

Ifc_RapidIO_RegisterManager rio_RegManager <- mkRapidIO_RegisterManager ();

// Rules -- 
rule rl_MapMaintenanceReqToRegManagerControl;
    rio_RegManager._inputs_mreq_SOF (rio_MainCore._MaintenanceReqInterface.mreq_sof_n_ ());
    rio_RegManager._inputs_mreq_EOF (rio_MainCore._MaintenanceReqInterface.mreq_eof_n_ ());
    rio_RegManager._inputs_mreq_VLD (rio_MainCore._MaintenanceReqInterface.mreq_vld_n_ ());
endrule 

rule rl_MapMaintenanceReqToRegManagerControl_Ready;
    rio_MainCore._MaintenanceReqInterface._mreq_rdy_n (rio_RegManager.outputs_mreq_rdy_ ());
endrule

rule rl_MapMaintenanceReqToRegManagerData;
    rio_RegManager._inputs_mreq_tt (rio_MainCore._MaintenanceReqInterface.mreq_tt_ ());
    rio_RegManager._inputs_mreq_data (rio_MainCore._MaintenanceReqInterface.mreq_data_ ());
    rio_RegManager._inputs_mreq_crf (rio_MainCore._MaintenanceReqInterface.mreq_crf_ ());
    rio_RegManager._inputs_mreq_prio (rio_MainCore._MaintenanceReqInterface.mreq_prio_ ());
    rio_RegManager._inputs_mreq_ftype (rio_MainCore._MaintenanceReqInterface.mreq_ftype_ ());
    rio_RegManager._inputs_mreq_ttype (rio_MainCore._MaintenanceReqInterface.mreq_ttype_ ());
    rio_RegManager._inputs_mreq_dest_id (rio_MainCore._MaintenanceReqInterface.mreq_dest_id_ ());
    rio_RegManager._inputs_mreq_source_id (rio_MainCore._MaintenanceReqInterface.mreq_source_id_ ());
    rio_RegManager._inputs_mreq_tid (rio_MainCore._MaintenanceReqInterface.mreq_tid_ ());
    rio_RegManager._inputs_mreq_offset (rio_MainCore._MaintenanceReqInterface.mreq_offset_ ());
    rio_RegManager._inputs_mreq_byte_en (rio_MainCore._MaintenanceReqInterface.mreq_byte_en_ ());
    rio_RegManager._inputs_mreq_byte_count (rio_MainCore._MaintenanceReqInterface.mreq_byte_count_ ());
    rio_RegManager._inputs_mreq_local (rio_MainCore._MaintenanceReqInterface.mreq_local_ ());

endrule 

rule rl_MapRegManagerToMaintenanceRespControl;
    rio_MainCore._MaintenanceRespInterface._mresp_sof_n (rio_RegManager.outputs_mresp_SOF_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_eof_n (rio_RegManager.outputs_mresp_EOF_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_vld_n (rio_RegManager.outputs_mresp_VLD_ ());
    rio_RegManager._inputs_mresp_rdy_n_ (rio_MainCore._MaintenanceRespInterface.mresp_rdy_n_ ());
endrule
rule rl_MapRegManagerToMaintenanceRespTT;
    rio_MainCore._MaintenanceRespInterface._mresp_tt (rio_RegManager.outputs_mresp_tt_ ());
endrule
rule rl_MapRegManagerToMaintenanceRespData;
    rio_MainCore._MaintenanceRespInterface._mresp_data (rio_RegManager.outputs_mresp_data_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_crf (rio_RegManager.outputs_mresp_crf_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_prio (rio_RegManager.outputs_mresp_prio_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_ftype (rio_RegManager.outputs_mresp_ftype_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_ttype (rio_RegManager.outputs_mresp_ttype_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_dest_id (rio_RegManager.outputs_mresp_dest_id_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_hop_count (rio_RegManager.outputs_mresp_hop_count_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_tid (rio_RegManager.outputs_mresp_tid_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_local (rio_RegManager.outputs_mresp_local_ ());
    rio_MainCore._MaintenanceRespInterface._mresp_status (rio_RegManager.outputs_mresp_status_ ());
endrule 
/*
rule rl_MResp_Ready_Dummy; // Dummy Interface 
    rio_MainCore._MaintenanceRespInterface._inputs_MRespRDYIn_From_Concat (False); 
endrule 
*/
// Interface Definitions
 // -- Initiator Request 
 interface Ifc_InitiatorReqIFC _InitReqInterface;
 method Action _ireq_sof_n (Bool value);
	rio_MainCore._InitReqInterface._ireq_sof_n (value);
 endmethod
 method Action _ireq_eof_n (Bool value);
	rio_MainCore._InitReqInterface._ireq_eof_n (value);
 endmethod
 method Action _ireq_vld_n (Bool value);
	rio_MainCore._InitReqInterface._ireq_vld_n (value);
 endmethod
 method Action _ireq_dsc_n (Bool value);
	rio_MainCore._InitReqInterface._ireq_dsc_n (value);
 endmethod
 method Bool ireq_rdy_n_ ();
	return (rio_MainCore._InitReqInterface.ireq_rdy_n_ ());
 endmethod

 method Action _ireq_tt (TT value);
	rio_MainCore._InitReqInterface._ireq_tt (value);
 endmethod 
 method Action _ireq_data (Data value);
	rio_MainCore._InitReqInterface._ireq_data (value);
 endmethod
 method Action _ireq_crf (Bool value);
	rio_MainCore._InitReqInterface._ireq_crf (value);
 endmethod
 method Action _ireq_prio (Prio value);
	rio_MainCore._InitReqInterface._ireq_prio (value);
 endmethod
 method Action _ireq_ftype (Type value);
	rio_MainCore._InitReqInterface._ireq_ftype (value);
 endmethod
 method Action _ireq_dest_id (DestId value);
	rio_MainCore._InitReqInterface._ireq_dest_id (value);
 endmethod
 method Action _ireq_addr (Addr value);
	rio_MainCore._InitReqInterface._ireq_addr (value);
 endmethod
 method Action _ireq_hopcount (Bit#(8) value);
	rio_MainCore._InitReqInterface._ireq_hopcount (value);
 endmethod
 method Action _ireq_tid (TranId value);
	rio_MainCore._InitReqInterface._ireq_tid (value);
 endmethod
 method Action _ireq_ttype (Type value);
	rio_MainCore._InitReqInterface._ireq_ttype (value);
 endmethod
 method Action _ireq_byte_count (ByteCount value);
	rio_MainCore._InitReqInterface._ireq_byte_count (value);
 endmethod
 method Action _ireq_byte_en_n (ByteEn value);
	rio_MainCore._InitReqInterface._ireq_byte_en_n (value);
 endmethod

 method Action _ireq_local (Bool value);
	rio_MainCore._InitReqInterface._ireq_local (value);
 endmethod
 method Action _ireq_db_info (DoorBell value);
	rio_MainCore._InitReqInterface._ireq_db_info (value);
 endmethod
 method Action _ireq_msg_len (MsgLen value);
	rio_MainCore._InitReqInterface._ireq_msg_len (value);
 endmethod
 method Action _ireq_msg_seg (MsgSeg value);
	rio_MainCore._InitReqInterface._ireq_msg_seg (value);
 endmethod
 method Action _ireq_mbox (Bit#(6) value);
	rio_MainCore._InitReqInterface._ireq_mbox (value);
 endmethod
 method Action _ireq_letter (Mletter value);
	rio_MainCore._InitReqInterface._ireq_letter (value);
 endmethod
/*
 method InitiatorReqIfcPkt outputs_InitReqIfcPkt_ ();
	return (rio_MainCore._InitReqInterface.outputs_InitReqIfcPkt_ ());
 endmethod

 method Action _inputs_IreqRDYIn_From_Concat (Bool value); // Dummy Interface 
    	rio_MainCore._InitReqInterface._inputs_IreqRDYIn_From_Concat (value); 
 endmethod
*/
 endinterface : _InitReqInterface

 // -- Target Response 
 interface Ifc_TargetRespIFC _TargetRespInterface;
 method Action _tresp_sof_n (Bool value);
	rio_MainCore._TargetRespInterface._tresp_sof_n (value);
 endmethod
 method Action _tresp_eof_n (Bool value); 
	rio_MainCore._TargetRespInterface._tresp_eof_n (value); 
 endmethod
 method Action _tresp_vld_n (Bool value);
	rio_MainCore._TargetRespInterface._tresp_vld_n (value);
 endmethod
 method Action _tresp_dsc_n (Bool value);
	rio_MainCore._TargetRespInterface._tresp_dsc_n (value);
 endmethod
 method Bool tresp_rdy_n_ ();
	return rio_MainCore._TargetRespInterface.tresp_rdy_n_ ();
 endmethod

 method Action _tresp_tt (TT value); 
	rio_MainCore._TargetRespInterface._tresp_tt (value);
 endmethod 
 method Action _tresp_data (Data value);
	rio_MainCore._TargetRespInterface._tresp_data (value);
 endmethod
 method Action _tresp_crf (Bool value);
	rio_MainCore._TargetRespInterface._tresp_crf (value);
 endmethod
 method Action _tresp_prio (Prio value);
	rio_MainCore._TargetRespInterface._tresp_prio (value);
 endmethod
 method Action _tresp_ftype (Type value);
	rio_MainCore._TargetRespInterface._tresp_ftype (value);
 endmethod
 method Action _tresp_dest_id (DestId value);
	rio_MainCore._TargetRespInterface._tresp_dest_id (value);
 endmethod
 method Action _tresp_status (Status value);
	rio_MainCore._TargetRespInterface._tresp_status (value);
 endmethod
 method Action _tresp_tid (TranId value);
	rio_MainCore._TargetRespInterface._tresp_tid (value);
 endmethod
 method Action _tresp_ttype (Type value);
	rio_MainCore._TargetRespInterface._tresp_ttype (value);
 endmethod
 method Action _tresp_no_data (Bool value);
	rio_MainCore._TargetRespInterface._tresp_no_data (value);
 endmethod

 method Action _tresp_msg_seg (MsgSeg value);
	rio_MainCore._TargetRespInterface._tresp_msg_seg (value);
 endmethod
 method Action _tresp_mbox (Bit#(2) value);
 	rio_MainCore._TargetRespInterface._tresp_mbox (value);
 endmethod
 method Action _tresp_letter (Mletter value);
	rio_MainCore._TargetRespInterface._tresp_letter (value);
 endmethod
/*
 method TargetRespIfcPkt outputs_TgtRespIfcPkt_ ();
	return rio_MainCore._TargetRespInterface.outputs_TgtRespIfcPkt_ ();
 endmethod

 method Action _inputs_TRespRDYIn_From_Concat (Bool value); // Dummy Interface 
	rio_MainCore._TargetRespInterface._inputs_TRespRDYIn_From_Concat (value);
 endmethod 	*/

 endinterface : _TargetRespInterface

 // -- Initiator Response 
 interface Ifc_InitiatorRespIFC _InitRespInterface;
 method Bool iresp_sof_n_ ();
	return rio_MainCore._InitRespInterface.iresp_sof_n_ ();
 endmethod
 method Bool iresp_eof_n_ ();
	return rio_MainCore._InitRespInterface.iresp_eof_n_ ();
 endmethod
 method Bool iresp_vld_n_ ();
	return rio_MainCore._InitRespInterface.iresp_vld_n_ ();
 endmethod
 method Action _iresp_rdy_n (Bool value);
	rio_MainCore._InitRespInterface._iresp_rdy_n (value);
 endmethod

 method TT iresp_tt_ (); 
	return rio_MainCore._InitRespInterface.iresp_tt_ ();
 endmethod 
 method Data iresp_data_ ();
	return rio_MainCore._InitRespInterface.iresp_data_ ();
 endmethod
 method Bool iresp_crf_ ();
	return rio_MainCore._InitRespInterface.iresp_crf_ ();
 endmethod
 method Prio iresp_prio_ ();
	return rio_MainCore._InitRespInterface.iresp_prio_ ();
 endmethod
 method Type iresp_ftype_ ();
	return rio_MainCore._InitRespInterface.iresp_ftype_ ();
 endmethod
 method Type iresp_ttype_ ();
	return rio_MainCore._InitRespInterface.iresp_ttype_ ();
 endmethod
 method DestId iresp_dest_id_ ();
	return rio_MainCore._InitRespInterface.iresp_dest_id_ ();
 endmethod
 method SourceId iresp_source_id_ ();
	return rio_MainCore._InitRespInterface.iresp_source_id_ ();
 endmethod
 method Status iresp_status_ ();
	return rio_MainCore._InitRespInterface.iresp_status_ ();  
 endmethod
 method TranId iresp_tid_ ();
	return rio_MainCore._InitRespInterface.iresp_tid_ ();
 endmethod 
 method Bool iresp_local_ ();
	return rio_MainCore._InitRespInterface.iresp_local_ ();
 endmethod

 method MsgSeg iresp_msg_seg_ ();
	return rio_MainCore._InitRespInterface.iresp_msg_seg_ ();
 endmethod
 method Bit#(2) iresp_mbox_ ();
	return rio_MainCore._InitRespInterface.iresp_mbox_ ();
 endmethod
 method Mletter iresp_letter_ ();
	return rio_MainCore._InitRespInterface.iresp_letter_ ();
 endmethod

 endinterface : _InitRespInterface

// -- Target Request 
 interface Ifc_TargetReqIFC _TargetReqInterface;
 method Bool treq_sof_n_ ();
	return rio_MainCore._TargetReqInterface.treq_sof_n_ ();
 endmethod
 method Bool treq_eof_n_ ();
	return rio_MainCore._TargetReqInterface.treq_eof_n_ ();
 endmethod
 method Bool treq_vld_n_ ();
	return rio_MainCore._TargetReqInterface.treq_vld_n_ ();
 endmethod
 method Action _treq_rdy_n (Bool value);
	rio_MainCore._TargetReqInterface._treq_rdy_n (value);
 endmethod

 method TT treq_tt_ (); 
	return rio_MainCore._TargetReqInterface.treq_tt_ ();
 endmethod 
 method Data treq_data_ ();
	return rio_MainCore._TargetReqInterface.treq_data_ ();
 endmethod
 method Bool treq_crf_ ();
	return rio_MainCore._TargetReqInterface.treq_crf_ ();
 endmethod
 method Prio treq_prio_ ();
	return rio_MainCore._TargetReqInterface.treq_prio_ ();
 endmethod
 method Type treq_ftype_ ();
	return rio_MainCore._TargetReqInterface.treq_ftype_ ();
 endmethod
 method DestId treq_dest_id_ ();
	return rio_MainCore._TargetReqInterface.treq_dest_id_ ();
 endmethod
 method SourceId treq_source_id_ ();
	return rio_MainCore._TargetReqInterface.treq_source_id_ ();
 endmethod
 method TranId treq_tid_ ();
	return rio_MainCore._TargetReqInterface.treq_tid_ ();
 endmethod
 method Type treq_ttype_ ();
	return rio_MainCore._TargetReqInterface.treq_ttype_ ();
 endmethod
 method Addr treq_addr_ ();
	return rio_MainCore._TargetReqInterface.treq_addr_ ();
 endmethod
 method ByteCount treq_byte_count_ ();
	return rio_MainCore._TargetReqInterface.treq_byte_count_ ();
 endmethod
 method ByteEn treq_byte_en_n_ ();
	return rio_MainCore._TargetReqInterface.treq_byte_en_n_ ();
 endmethod

 method DoorBell treq_db_info_ ();
	return rio_MainCore._TargetReqInterface.treq_db_info_ ();
 endmethod
 method MsgLen treq_msg_len_ ();
	return rio_MainCore._TargetReqInterface.treq_msg_len_ ();
 endmethod
 method MsgSeg treq_msg_seg_ ();
	return rio_MainCore._TargetReqInterface.treq_msg_seg_ ();
 endmethod
 method Bit#(6) treq_mbox_ ();
	return rio_MainCore._TargetReqInterface.treq_mbox_ ();
 endmethod
 method Mletter treq_letter_ ();
	return rio_MainCore._TargetReqInterface.treq_letter_ ();
 endmethod

 endinterface : _TargetReqInterface

 // -- Transmit Packet - Link Interface 
 interface Ifc_LinkInterfaceTx _TxPktsLinkIfc;
 method Bool link_tx_sof_n_ ();
	return rio_MainCore._TxPktsLinkIfc.link_tx_sof_n_ ();
 endmethod
 method Bool link_tx_eof_n_ ();
	return rio_MainCore._TxPktsLinkIfc.link_tx_eof_n_ ();
 endmethod
 method Bool link_tx_vld_n_ ();
	return rio_MainCore._TxPktsLinkIfc.link_tx_vld_n_ ();
 endmethod
 method Bool link_tx_dsc_n_ ();
	return rio_MainCore._TxPktsLinkIfc.link_tx_dsc_n_ ();
 endmethod
 method Action link_tx_rdy_n (Bool value);
	rio_MainCore._TxPktsLinkIfc.link_tx_rdy_n (value);
 endmethod

 //-- Data Signals
 method DataPkt link_tx_data_ ();
	return rio_MainCore._TxPktsLinkIfc.link_tx_data_ ();
 endmethod
 method Bit#(4) link_tx_rem_ ();
	return rio_MainCore._TxPktsLinkIfc.link_tx_rem_ ();
 endmethod
 method Bool link_tx_crf_ ();
	return rio_MainCore._TxPktsLinkIfc.link_tx_crf_ ();
 endmethod
 method Action link_tx_master_enable (Bool value); 
	rio_MainCore._TxPktsLinkIfc.link_tx_master_enable (value); 
 endmethod

 endinterface : _TxPktsLinkIfc

 // -- Receive Packet - Link Interface 
 interface Ifc_LinkInterfaceRx _RxPktsLinkIfc;
 method Action _link_rx_sof_n (Bool value);
 	rio_MainCore._RxPktsLinkIfc._link_rx_sof_n (value);
 endmethod
 method Action _link_rx_eof_n (Bool value);
	rio_MainCore._RxPktsLinkIfc._link_rx_eof_n (value);
 endmethod
 method Action _link_rx_vld_n (Bool value);
	rio_MainCore._RxPktsLinkIfc._link_rx_vld_n (value);
 endmethod
 method Bool link_rx_rdy_n_ ();
	return rio_MainCore._RxPktsLinkIfc.link_rx_rdy_n_ ();
 endmethod

 method Action _link_rx_data (DataPkt value);
	rio_MainCore._RxPktsLinkIfc._link_rx_data (value);
 endmethod
 method Action _link_rx_rem (Bit#(4) value);
	rio_MainCore._RxPktsLinkIfc._link_rx_rem (value);
 endmethod
 method Action _link_rx_crf (Bool value);
	rio_MainCore._RxPktsLinkIfc._link_rx_crf (value);
 endmethod

 endinterface : _RxPktsLinkIfc

endmodule : mkRapidIO_TopModule 






endpackage : RapidIO_TopModule

