/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Top Wrapper Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This module is designed to map the RapidIO Top Module and RapidIO Target Operations. 
-- When Initiator Request is sen thro the Initiator Req signals. The Target receives the request 
-- through the Target Req signals. The Target performs the request operations and send the responses 
-- through the Target Response signals.  
-- This is the Top Module. 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_Top_Wrapper_Loop;

import RapidIO_DTypes ::*;
import RapidIO_TopModule ::*;
import RapidIO_TgtOperations ::*;
import RapidIO_LoopVerification ::*;

/*
-- Input and Output Interface :
-- Input : Initiator Request, Receive Link Packets
-- Output : Initiator Response, Transmit Link Packets 
*/

interface Ifc_RapidIO_Top_Wrapper_Loop;

 interface Ifc_InitiatorReqIFC _TWIReqInterface;
 interface Ifc_InitiatorRespIFC _TWIRespInterface;

// interface Ifc_LinkInterfaceTx _TWTxPktsLinkIfc;
// interface Ifc_LinkInterfaceRx _TWRxPktsLinkIfc;

endinterface : Ifc_RapidIO_Top_Wrapper_Loop


(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_Top_Wrapper_Loop (Ifc_RapidIO_Top_Wrapper_Loop);

Ifc_RapidIO_TopModule rio_Top_Module <- mkRapidIO_TopModule ();
Ifc_RapidIO_TgtOperations rio_TgtOperations <- mkRapidIO_TgtOperations ();
Ifc_RapidIO_LoopVerification rio_LoopModule <- mkRapidIO_LoopVerification ();


// -- Rules -- 
/*
-- Following rule, it is used to map the Target Request signals from RapidIO to perform Target Request
-- operations. All control and Data signals are mapped to Tgt operations module. 
-- NOTE : Message Signals are kept unused. 
*/
rule rl_Map_Target_Request_Inputs; 
    rio_TgtOperations.inputs_treq_sof (rio_Top_Module._TargetReqInterface.treq_sof_n_ ());
    rio_TgtOperations.inputs_treq_eof (rio_Top_Module._TargetReqInterface.treq_eof_n_ ());
    rio_TgtOperations.inputs_treq_vld (rio_Top_Module._TargetReqInterface.treq_vld_n_ ());

 //-- Data Signal Interface
    rio_TgtOperations.inputs_treq_tt (rio_Top_Module._TargetReqInterface.treq_tt_ ());
    rio_TgtOperations.inputs_treq_data (rio_Top_Module._TargetReqInterface.treq_data_ ());
    rio_TgtOperations.inputs_treq_crf (rio_Top_Module._TargetReqInterface.treq_crf_ ());
    rio_TgtOperations.inputs_treq_prio (rio_Top_Module._TargetReqInterface.treq_prio_ ());
    rio_TgtOperations.inputs_treq_ftype (rio_Top_Module._TargetReqInterface.treq_ftype_ ());
    rio_TgtOperations.inputs_treq_dest_id (rio_Top_Module._TargetReqInterface.treq_dest_id_ ());
    rio_TgtOperations.inputs_treq_source_id (rio_Top_Module._TargetReqInterface.treq_source_id_ ());
    rio_TgtOperations.inputs_treq_tid (rio_Top_Module._TargetReqInterface.treq_tid_ ());
    rio_TgtOperations.inputs_treq_ttype (rio_Top_Module._TargetReqInterface.treq_ttype_ ());
    rio_TgtOperations.inputs_treq_addr (rio_Top_Module._TargetReqInterface.treq_addr_ ());
    rio_TgtOperations.inputs_treq_byte_count (rio_Top_Module._TargetReqInterface.treq_byte_count_ ());
    rio_TgtOperations.inputs_treq_byte_en (rio_Top_Module._TargetReqInterface.treq_byte_en_n_ ());

 //-- Message Signal Interface
    rio_TgtOperations.inputs_treq_db_info (rio_Top_Module._TargetReqInterface.treq_db_info_ ());
    rio_TgtOperations.inputs_treq_msg_len (rio_Top_Module._TargetReqInterface.treq_msg_len_ ());
    rio_TgtOperations.inputs_treq_msg_seg (rio_Top_Module._TargetReqInterface.treq_msg_seg_ ());
    rio_TgtOperations.inputs_treq_mbox (rio_Top_Module._TargetReqInterface.treq_mbox_ ());
    rio_TgtOperations.inputs_treq_letter (rio_Top_Module._TargetReqInterface.treq_letter_ ());
endrule 

/*
-- Following rule, it is used to map the Ready signal from Target operations module to RapidIO 
*/
rule rl_Map_Tgt_ReadySignal;
    rio_Top_Module._TargetReqInterface._treq_rdy_n (rio_TgtOperations.outputs_treq_rdy_n ());
endrule 

/*
-- Following rule, it is used to map Target Response signals from Tgt operations module to RapidIO.
-- All control and Data signals are mapped. 
-- NOTE : Message Signals are kept unused. 
*/
rule rl_Map_Target_Response_Outputs;
     //-- Control Signal Interface
    rio_Top_Module._TargetRespInterface._tresp_sof_n (rio_TgtOperations.outputs_tresp_sof_ ());
    rio_Top_Module._TargetRespInterface._tresp_eof_n (rio_TgtOperations.outputs_tresp_eof_ ()); 
    rio_Top_Module._TargetRespInterface._tresp_vld_n (rio_TgtOperations.outputs_tresp_vld_ ());
    rio_Top_Module._TargetRespInterface._tresp_dsc_n (True);
    rio_TgtOperations._inputs_tresp_rdy_n_(rio_Top_Module._TargetRespInterface.tresp_rdy_n_ ());

 //-- Data Signal Interface
    rio_Top_Module._TargetRespInterface._tresp_tt (rio_TgtOperations.outputs_tresp_tt_ ());
    rio_Top_Module._TargetRespInterface._tresp_data (rio_TgtOperations.outputs_tresp_data_ ());
    rio_Top_Module._TargetRespInterface._tresp_crf (rio_TgtOperations.outputs_tresp_crf_ ());
    rio_Top_Module._TargetRespInterface._tresp_prio (rio_TgtOperations.outputs_tresp_prio_ ());
    rio_Top_Module._TargetRespInterface._tresp_ftype (rio_TgtOperations.outputs_tresp_ftype_ ());
    rio_Top_Module._TargetRespInterface._tresp_dest_id (rio_TgtOperations.outputs_tresp_dest_id_ ());
    rio_Top_Module._TargetRespInterface._tresp_status (rio_TgtOperations.outputs_tresp_status_ ());
    rio_Top_Module._TargetRespInterface._tresp_tid (rio_TgtOperations.outputs_tresp_tid_ ());
    rio_Top_Module._TargetRespInterface._tresp_ttype (rio_TgtOperations.outputs_tresp_ttype_ ());
    rio_Top_Module._TargetRespInterface._tresp_no_data (rio_TgtOperations.outputs_tresp_no_data_ ());

 //-- Message Signal Interface
    rio_Top_Module._TargetRespInterface._tresp_msg_seg (rio_TgtOperations.outputs_tresp_msg_seg_ ());
    rio_Top_Module._TargetRespInterface._tresp_mbox (rio_TgtOperations.outputs_tresp_mbox_ ());
    rio_Top_Module._TargetRespInterface._tresp_letter (rio_TgtOperations.outputs_tresp_letter ());
endrule

rule rl_Connect_TxSignals_Loop; 
	rio_LoopModule._intx_sof_n (rio_Top_Module._TxPktsLinkIfc.link_tx_sof_n_ ());
	rio_LoopModule._intx_eof_n (rio_Top_Module._TxPktsLinkIfc.link_tx_eof_n_ ());
        rio_LoopModule._intx_vld_n (rio_Top_Module._TxPktsLinkIfc.link_tx_vld_n_ ());
	rio_LoopModule._intx_dsc_n (rio_Top_Module._TxPktsLinkIfc.link_tx_dsc_n_ ());
	rio_LoopModule._intx_data (rio_Top_Module._TxPktsLinkIfc.link_tx_data_ ());
	rio_LoopModule._intx_rem (rio_Top_Module._TxPktsLinkIfc.link_tx_rem_ ());
	rio_LoopModule._intx_crf (rio_Top_Module._TxPktsLinkIfc.link_tx_crf_ ());
endrule
rule rl_Connect_TxRdy_LoopModule;
	rio_Top_Module._TxPktsLinkIfc.link_tx_rdy_n (rio_LoopModule.outtx_rdy_n_ ());
endrule 

rule rl_Connect_TxMaster_LoopModule;
	rio_Top_Module._TxPktsLinkIfc.link_tx_master_enable (rio_LoopModule.outtx_master_enable_ ()); 
endrule
 
rule rl_Connect_LoopModule_RxSignals; 
	rio_Top_Module._RxPktsLinkIfc._link_rx_sof_n (rio_LoopModule.out_rx_sof_n_ ());
	rio_Top_Module._RxPktsLinkIfc._link_rx_eof_n (rio_LoopModule.out_rx_eof_n_ ());
	rio_Top_Module._RxPktsLinkIfc._link_rx_vld_n (rio_LoopModule.out_rx_vld_n_ ());
	rio_Top_Module._RxPktsLinkIfc._link_rx_data (rio_LoopModule.out_rx_data_());
	rio_Top_Module._RxPktsLinkIfc._link_rx_rem (rio_LoopModule.out_rx_rem_ ());
	rio_Top_Module._RxPktsLinkIfc._link_rx_crf (rio_LoopModule.out_rx_crf_ ());
endrule 
rule rl_Connect_RxRdy_Rxsignals;
	rio_LoopModule.in_rx_rdy_n(rio_Top_Module._RxPktsLinkIfc.link_rx_rdy_n_ ());
endrule 

// Methods Definitions. 
 interface Ifc_InitiatorReqIFC _TWIReqInterface;
 //--  Control Signal interface
 method Action _ireq_sof_n (Bool value); 
	rio_Top_Module._InitReqInterface._ireq_sof_n (value);
 endmethod 
 method Action _ireq_eof_n (Bool value);
	rio_Top_Module._InitReqInterface._ireq_eof_n (value);
 endmethod 
 method Action _ireq_vld_n (Bool value);
	rio_Top_Module._InitReqInterface._ireq_vld_n (value);
 endmethod 
 method Action _ireq_dsc_n (Bool value);
	rio_Top_Module._InitReqInterface._ireq_dsc_n (value);
 endmethod 
 method Bool ireq_rdy_n_ ();
	return rio_Top_Module._InitReqInterface.ireq_rdy_n_ ();
 endmethod 

 //-- Data Signal Interface
 method Action _ireq_tt (TT value);
	rio_Top_Module._InitReqInterface._ireq_tt (value);
 endmethod 
 method Action _ireq_data (Data value);
	rio_Top_Module._InitReqInterface._ireq_data (value);
 endmethod 
 method Action _ireq_crf (Bool value);
	rio_Top_Module._InitReqInterface._ireq_crf (value);
 endmethod 
 method Action _ireq_prio (Prio value);
	rio_Top_Module._InitReqInterface._ireq_prio (value);
 endmethod 
 method Action _ireq_ftype (Type value);
	rio_Top_Module._InitReqInterface._ireq_ftype (value);
 endmethod 
 method Action _ireq_dest_id (DestId value);
	rio_Top_Module._InitReqInterface._ireq_dest_id (value);
 endmethod 
 method Action _ireq_addr (Addr value);
	rio_Top_Module._InitReqInterface._ireq_addr (value);
 endmethod 
 method Action _ireq_hopcount (Bit#(8) value);
	rio_Top_Module._InitReqInterface._ireq_hopcount (value);
 endmethod 
 method Action _ireq_tid (TranId value);
	rio_Top_Module._InitReqInterface._ireq_tid (value);
 endmethod 
 method Action _ireq_ttype (Type value);
	rio_Top_Module._InitReqInterface._ireq_ttype (value);
 endmethod 
 method Action _ireq_byte_count (ByteCount value);
	rio_Top_Module._InitReqInterface._ireq_byte_count (value);
 endmethod 
 method Action _ireq_byte_en_n (ByteEn value);
	rio_Top_Module._InitReqInterface._ireq_byte_en_n (value);
 endmethod 

 //-- Message Signal Interface
 method Action _ireq_local (Bool value);
	rio_Top_Module._InitReqInterface._ireq_local (value);
 endmethod 
 method Action _ireq_db_info (DoorBell value);
	rio_Top_Module._InitReqInterface._ireq_db_info (value);
 endmethod 
 method Action _ireq_msg_len (MsgLen value);
	rio_Top_Module._InitReqInterface._ireq_msg_len (value);
 endmethod 
 method Action _ireq_msg_seg (MsgSeg value);
	rio_Top_Module._InitReqInterface._ireq_msg_seg (value);
 endmethod 
 method Action _ireq_mbox (Bit#(6) value);
	rio_Top_Module._InitReqInterface._ireq_mbox (value);
 endmethod 
 method Action _ireq_letter (Mletter value);
	rio_Top_Module._InitReqInterface._ireq_letter (value);
 endmethod 

 //-- Initiator Signal
// method InitiatorReqIfcPkt outputs_InitReqIfcPkt_ ();
// method Action _inputs_IreqRDYIn_From_Concat (Bool value);
 endinterface : _TWIReqInterface

 interface Ifc_InitiatorRespIFC _TWIRespInterface;
 //-- Control Signal Interface
 method Bool iresp_sof_n_ ();
	return rio_Top_Module._InitRespInterface.iresp_sof_n_ ();
 endmethod 
 method Bool iresp_eof_n_ ();
	return rio_Top_Module._InitRespInterface.iresp_eof_n_ ();
 endmethod 
 method Bool iresp_vld_n_ ();
	return rio_Top_Module._InitRespInterface.iresp_vld_n_ ();
 endmethod 
 method Action _iresp_rdy_n (Bool value);
	rio_Top_Module._InitRespInterface._iresp_rdy_n (value);
 endmethod 

 //-- Data Signal Interface
 method TT iresp_tt_ ();
	return rio_Top_Module._InitRespInterface.iresp_tt_ ();
 endmethod 
 method Data iresp_data_ ();
	return rio_Top_Module._InitRespInterface.iresp_data_ ();
 endmethod
 method Bool iresp_crf_ ();
	return rio_Top_Module._InitRespInterface.iresp_crf_ ();
 endmethod 
 method Prio iresp_prio_ ();
	return rio_Top_Module._InitRespInterface.iresp_prio_ ();
 endmethod 
 method Type iresp_ftype_ ();
	return rio_Top_Module._InitRespInterface.iresp_ftype_ ();
 endmethod 
 method Type iresp_ttype_ ();
	return rio_Top_Module._InitRespInterface.iresp_ttype_ ();
 endmethod 
 method DestId iresp_dest_id_ ();
	return rio_Top_Module._InitRespInterface.iresp_dest_id_ ();
 endmethod 
 method SourceId iresp_source_id_ ();
	return rio_Top_Module._InitRespInterface.iresp_source_id_ ();
 endmethod 
 method Status iresp_status_ ();
	return rio_Top_Module._InitRespInterface.iresp_status_ ();
 endmethod 
 method TranId iresp_tid_ ();
	return rio_Top_Module._InitRespInterface.iresp_tid_ ();
 endmethod 

 //-- Message Signal Interface
 method Bool iresp_local_ ();
	return rio_Top_Module._InitRespInterface.iresp_local_ ();
 endmethod 
 method MsgSeg iresp_msg_seg_ ();
	return rio_Top_Module._InitRespInterface.iresp_msg_seg_ ();
 endmethod 
 method Bit#(2) iresp_mbox_ ();
	return rio_Top_Module._InitRespInterface.iresp_mbox_ ();
 endmethod 
 method Mletter iresp_letter_ ();
	return rio_Top_Module._InitRespInterface.iresp_letter_ ();
 endmethod 
 endinterface : _TWIRespInterface
/*
 interface Ifc_LinkInterfaceTx _TWTxPktsLinkIfc;
 //-- Control Signals
 method Bool link_tx_sof_n_ ();
	return rio_Top_Module._TxPktsLinkIfc.link_tx_sof_n_ ();
 endmethod 
 method Bool link_tx_eof_n_ ();
	return rio_Top_Module._TxPktsLinkIfc.link_tx_eof_n_ ();
 endmethod
 method Bool link_tx_vld_n_ ();
	return rio_Top_Module._TxPktsLinkIfc.link_tx_vld_n_ ();
 endmethod 
 method Bool link_tx_dsc_n_ ();
	return rio_Top_Module._TxPktsLinkIfc.link_tx_dsc_n_ ();
 endmethod 
 method Action link_tx_rdy_n (Bool value);
	rio_Top_Module._TxPktsLinkIfc.link_tx_rdy_n (value);
 endmethod 

 //-- Data Signals
 method Data link_tx_data_ ();
	return rio_Top_Module._TxPktsLinkIfc.link_tx_data_ ();
 endmethod 
 method Bit#(3) link_tx_rem_ ();
	return rio_Top_Module._TxPktsLinkIfc.link_tx_rem_ ();
 endmethod 
 method Bool link_tx_crf_ ();
	return rio_Top_Module._TxPktsLinkIfc.link_tx_crf_ ();
 endmethod 
 method Action link_tx_master_enable (Bool value);
	rio_Top_Module._TxPktsLinkIfc.link_tx_master_enable (value); 
 endmethod 
 endinterface : _TWTxPktsLinkIfc

 interface Ifc_LinkInterfaceRx _TWRxPktsLinkIfc;
 //-- Control Signals
 method Action _link_rx_sof_n (Bool value);
	rio_Top_Module._RxPktsLinkIfc._link_rx_sof_n (value);
 endmethod 
 method Action _link_rx_eof_n (Bool value);
	rio_Top_Module._RxPktsLinkIfc._link_rx_eof_n (value);
 endmethod 
 method Action _link_rx_vld_n (Bool value);
	rio_Top_Module._RxPktsLinkIfc._link_rx_vld_n (value);
 endmethod 
 method Bool link_rx_rdy_n_ ();
	return rio_Top_Module._RxPktsLinkIfc.link_rx_rdy_n_ ();
 endmethod 

 //-- Data Signals
 method Action _link_rx_data (Data value);
	rio_Top_Module._RxPktsLinkIfc._link_rx_data (value);
 endmethod 
 method Action _link_rx_rem (Bit#(3) value);
	rio_Top_Module._RxPktsLinkIfc._link_rx_rem (value);
 endmethod 
 method Action _link_rx_crf (Bool value);
	rio_Top_Module._RxPktsLinkIfc._link_rx_crf (value);
 endmethod 
 endinterface : _TWRxPktsLinkIfc
*/
endmodule : mkRapidIO_Top_Wrapper_Loop 

endpackage : RapidIO_Top_Wrapper_Loop
