/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Register File Manager Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module contains the Capability Registers (CARs) and Command and Status Registers (CSRs) 
--
--
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------

-- Configuration Space Byte Offset 		Registers
-- 	0x0					Device Identity CAR 
--	0x4					Device Information CAR
-- 	0x8					Assembly Identity CAR 
-- 	0xC					Assembly Information CAR
-- 	0x10					Processing Element Features CAR
--	0x14					Switch Port Information CAR
-- 	0x18					Source Operations CAR
-- 	0x1C 					Destination Operations CAR
-- 	0x20 - 0x48				-- Reserved -- 
-- 	0x4C					Processing Element Logical Layer Control (LLC) CSR
-- 	0x50					-- Reserved -- 
-- 	0x58					Local Configuration Space Base Address 0 (SBA0) CSR
-- 	0x5C					Local Configuration Space Base Address 1 CSR
--	0x60 - 0xFC				-- Reserved -- 
--
-------------------------------------------------------------------------------
*/

package RapidIO_RegisterManager;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_RegisterFiles ::*;

`include "RapidIO_RegisterFile_Offset.defines"

/*
-- Maintenance Request Signals are given as input signals to the Register Manager and 
-- Maintenance Response signals are generated in the Register Manager module. 
-- The Maintenance Request packet is generated using Initiator Request signals and the 
-- Maintenance Response packet is decoded and sent to Initiator Response interface.  
*/

interface Ifc_RapidIO_RegisterManager;

 //-- Maintenance Request Control Interface - Input Methods 
 method Action _inputs_mreq_SOF (Bool value);
 method Action _inputs_mreq_EOF (Bool value);
 method Action _inputs_mreq_VLD (Bool value);

 method Bool outputs_mreq_rdy_ (); // Output

 //-- Maintenance Request Data Interface
 method Action _inputs_mreq_tt (TT value);
 method Action _inputs_mreq_data (Data value);
 method Action _inputs_mreq_crf (Bool value);
 method Action _inputs_mreq_prio (Prio value);
 method Action _inputs_mreq_ftype (Type value);
 method Action _inputs_mreq_ttype (Type value);
 method Action _inputs_mreq_dest_id (DestId value);
 method Action _inputs_mreq_source_id (SourceId value);
 method Action _inputs_mreq_tid (TranId value);
 method Action _inputs_mreq_offset (Offset value);
 method Action _inputs_mreq_byte_en (ByteEn value);
 method Action _inputs_mreq_byte_count (ByteCount value);
 method Action _inputs_mreq_local (Bool value);

// -- Maintenance Response Signals - Output Methods

 //-- Maintenance Response Control Interface
 method Bool outputs_mresp_SOF_ ();
 method Bool outputs_mresp_EOF_ ();
 method Bool outputs_mresp_VLD_ ();

 method Action _inputs_mresp_rdy_n_ (Bool value); // Input 

 //-- Maintenance Response Data Interface
 method TT outputs_mresp_tt_ ();
 method Data outputs_mresp_data_ ();
 method Bool outputs_mresp_crf_ ();
 method Prio outputs_mresp_prio_ ();
 method Type outputs_mresp_ftype_ ();
 method Type outputs_mresp_ttype_ ();
 method DestId outputs_mresp_dest_id_ ();
 method Bit#(8) outputs_mresp_hop_count_ ();
 method TranId outputs_mresp_tid_ ();
 method Bool outputs_mresp_local_ ();
 method Status outputs_mresp_status_ ();

endinterface : Ifc_RapidIO_RegisterManager

(* synthesize *)
//(* always_enabled *)
//(* always_ready *)
module mkRapidIO_RegisterManager (Ifc_RapidIO_RegisterManager);

// Input Methods as Wires
Wire#(Maybe#(Bool)) wr_Mreq_SOF <- mkDWire (tagged Invalid);
Wire#(Maybe#(Bool)) wr_Mreq_EOF <- mkDWire (tagged Invalid);
Wire#(Maybe#(Bool)) wr_Mreq_VLD <- mkDWire (tagged Invalid);

// method Action _mreq_rdy_n (Bool value); // Output

 //-- Maintenance Request Data Interface
Wire#(TT) wr_Mreq_tt <- mkDWire (0);
Wire#(Data) wr_Mreq_data <- mkDWire (0);
Wire#(Bool) wr_Mreq_crf <- mkDWire (False);
Wire#(Prio) wr_Mreq_prio <- mkDWire (0);
Wire#(Type) wr_Mreq_ftype <- mkDWire (0);
Wire#(Type) wr_Mreq_ttype <- mkDWire (0);
Wire#(DestId) wr_Mreq_destid <- mkDWire (0);
Wire#(SourceId) wr_Mreq_sourceid <- mkDWire (0);
Wire#(TranId) wr_Mreq_tid <- mkDWire (0);
Wire#(Offset) wr_Mreq_offset <- mkDWire (0);
Wire#(ByteEn) wr_Mreq_byte_en <- mkDWire (0);
Wire#(ByteCount) wr_Mreq_byte_count <- mkDWire (0);
Wire#(Bool) wr_Mreq_local <- mkDWire (False);

/*
-- Capability Registers are Read only Registers 
*/

// Capability Registers (CARs)
Reg#(RegSize) rg_Dev_Identity_CAR <- mkReg (0); 
Reg#(RegSize) rg_Dev_Info_CAR <- mkReg (0);
Reg#(RegSize) rg_Assembly_Identity_CAR <- mkReg (0);
Reg#(RegSize) rg_Assembly_Info_CAR <- mkReg (0);
Reg#(RegSize) rg_Proc_Element_Features_CAR <- mkReg (0);
Reg#(RegSize) rg_Switch_Port_Info_CAR <- mkReg (0);
Reg#(RegSize) rg_Source_Operations_CAR <- mkReg (0);
Reg#(RegSize) rg_Dest_Operations_CAR <- mkReg (0);

/*
-- Command and Status Registers are Read-Write Registers
-- Read operation is same as Capability Registers 
*/

// Command and Status Registers (CSRs) 
Reg#(RegSize) rg_Proc_Element_LLC_CSR <- mkReg (0); // LLC - Logical Layer Control CSR 
Reg#(RegSize) rg_Local_Config_SBA0_CSR <- mkReg (0); // SBA - Space Base Address 0 CSR 
Reg#(RegSize) rg_Local_Config_SBA1_CSR <- mkReg (0); // SBA - Space Base Address 1 CSR 

// Internal Wires and Registers
Wire#(Bool) wr_Read <- mkDWire (False); // Read Operation 
Wire#(Bool) wr_Write <- mkDWire (False); // Write Operation 
Wire#(Bit#(32)) wr_RFDataOut <- mkDWire (0); // Used to hold the Output data of the Register Files module
Reg#(Type) rg_TtypeResp <- mkReg (0); // Hold the Ttype field of the Maintenance Response packet 
Wire#(Status) wr_Status <- mkDWire (4'b0111); // Maintenance Response Status field (Generated in Register Files module)
Reg#(SourceId) rg_Mreq_sourceid <- mkReg (0); // Hold for 1 clock and sent back as Response 
Reg#(DestId) rg_Mreq_destid <- mkReg (0); // Hold for 1 clock and sent back as Response
Reg#(TranId) rg_Mreq_tid <- mkReg (0); // Hold for 1 clock and sent back as Response
Wire#(Bool) wr_Mresp_rdy <- mkDWire (False);
Reg#(Maybe#(Bool)) rg_Mreq_SOF <- mkReg (tagged Invalid); // Maintenance Request SOF field is delayed for 1 clock cycle. 
Reg#(ByteEn) rg_Mreq_byte_en <- mkReg (0); // Byte Enable is held for a cycle to serve as input to generate Response data 
Reg#(Bool) wr_ReadyOut <- mkReg (False);
Wire#(Bool) wr_Mreq_SOF_Val <- mkDWire (False);
Reg#(Prio) rg_Mresp_Prio <- mkReg (0);
Reg#(TT) rg_Mresp_tt <- mkReg (0); 


// Output Methods as Wires
Wire#(Bool) wr_Mresp_SOF <- mkDWire (False);

/*
-- Register File module is invoked to perform Read and Write operation to read and update the
-- RapidIO Capability Register (CAR) and Command and Status Register (CSR)
-- This module takes 1 clock cycle to read these registers
*/

// Module Instantiation 
Ifc_RapidIO_RegisterFiles mod_RegFiles <- mkRapidIO_RegisterFiles ();

// Rules 
/*
-- Following rule is used to determine whether the received Maintenance Request is to perform read or write operation.
-- Depend on the Ttype value, it determines the Read or Write operation. 
*/
rule rl_Determine_RdWrOperation;
    if (wr_Mreq_SOF matches tagged Valid .x) begin
        rg_Mresp_tt <= wr_Mreq_tt; 
	rg_Mresp_Prio <= wr_Mreq_prio + 1;  // Response Priority field must be incremented by 1. 
	if (wr_Mreq_ttype == 'd0) // Read 
	    wr_Read <= True; 
	else if (wr_Mreq_ttype == 'd1) // Write
	    wr_Write <= True; 
    end 
    else begin
        rg_Mresp_tt <= 0; 
	wr_Read <= False;
	wr_Write <= False;
	rg_Mresp_Prio <= 0;  
    end     
endrule

/*
-- Following rule is used to generate the Ttype field value for the Maintenance Response packets
-- using the received Ttype field.
*/

rule rl_TtpeRespGeneration;
    if (wr_Mreq_ttype == 4'h0) // Read Request 
	rg_TtypeResp <= 4'h2; // Read Response 
    else if (wr_Mreq_ttype == 4'h1) // Write Request 
	rg_TtypeResp <= 4'h3; // Write Response
    else 
	rg_TtypeResp <= 0; 
endrule 

/*
-- Following rule is used to supply inputs to the Register Files module.
*/

rule rl_RegFileInput; 
    mod_RegFiles._inputs_RIORead (wr_Read);
    mod_RegFiles._inputs_RIOWrite (wr_Write);
    mod_RegFiles._inputs_Offset (wr_Mreq_offset);
    mod_RegFiles._inputs_DataIn (wr_Mreq_data);
endrule 

/*
-- Following rule is used to receive the output from the Register File module when the 
-- Maintenance Request SOF (Delayed by 1 clock) is enabled.
-- During the 1 clock delay, the Register File module performs read/write operation and 
-- generates output.  
*/
rule rl_RegFileOutput;
    if (rg_Mreq_SOF matches tagged Valid .x) begin
	wr_Mresp_SOF <= True; 
  	wr_RFDataOut <= mod_RegFiles.outputs_DataOut_ ();
    	wr_Status <= mod_RegFiles.outputs_Status_ ();
	end
    else begin
	wr_Mresp_SOF <= False; 
	wr_RFDataOut <= 0;
    	wr_Status <= 0;
	end 
endrule 

rule  rl_Generate_ReadyOut;
     if (wr_Mreq_SOF_Val == True)
	wr_ReadyOut <= True; 
     else if (wr_Mresp_SOF == True)
	wr_ReadyOut <= True; 
     else 
	wr_ReadyOut <= False; 
endrule

// Methods Definition 
 //-- Maintenance Request Control Interface - Input Methods 
 method Action _inputs_mreq_SOF (Bool value);
    wr_Mreq_SOF_Val <= !value; 
    if (value == False) begin
	wr_Mreq_SOF <= tagged Valid (!value);
	rg_Mreq_SOF <= tagged Valid (!value);
	end 
    else begin
	wr_Mreq_SOF <= tagged Invalid;  
	rg_Mreq_SOF <= tagged Invalid; 
	end 
 endmethod
 method Action _inputs_mreq_EOF (Bool value);
    if (value == False)
	wr_Mreq_EOF <= tagged Valid (!value);
    else 
	wr_Mreq_EOF <= tagged Invalid;
 endmethod
 method Action _inputs_mreq_VLD (Bool value);
    if (value == False)
	wr_Mreq_VLD <= tagged Valid (!value);
    else 
	wr_Mreq_VLD <= tagged Invalid;
 endmethod

 method Bool outputs_mreq_rdy_ (); // Output
	return !wr_ReadyOut; 
 endmethod 

/*
-- Input signals are given only when Maintenance Request SOF signal is valid. 
*/
 //-- Maintenance Request Data Interface
 method Action _inputs_mreq_tt (TT value);
	wr_Mreq_tt <= value; 
 endmethod 
 method Action _inputs_mreq_data (Data value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_data <= value; 
 endmethod
 method Action _inputs_mreq_crf (Bool value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_crf <= value;
 endmethod
 method Action _inputs_mreq_prio (Prio value);
	wr_Mreq_prio <= value;
 endmethod
 method Action _inputs_mreq_ftype (Type value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_ftype <= value;
 endmethod
 method Action _inputs_mreq_ttype (Type value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_ttype <= value;
 endmethod
 method Action _inputs_mreq_dest_id (DestId value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_destid <= value;
	rg_Mreq_destid <= value; 
 endmethod
 method Action _inputs_mreq_source_id (SourceId value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_sourceid <= value;
	rg_Mreq_sourceid <= value; 
 endmethod
 method Action _inputs_mreq_tid (TranId value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_tid <= value;
	rg_Mreq_tid <= value; 
 endmethod
 method Action _inputs_mreq_offset (Offset value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_offset <= value;
 endmethod
 method Action _inputs_mreq_byte_en (ByteEn value) if (wr_Mreq_EOF matches tagged Valid .x);
	wr_Mreq_byte_en <= value;
	rg_Mreq_byte_en <= value; 
 endmethod
 method Action _inputs_mreq_byte_count (ByteCount value) if (wr_Mreq_SOF matches tagged Valid .x);
	wr_Mreq_byte_count <= value;
 endmethod
 method Action _inputs_mreq_local (Bool value);
	wr_Mreq_local <= value;
 endmethod


// -- Maintenance Response Signals - Output Methods

 //-- Maintenance Response Control Interface
 method Bool outputs_mresp_SOF_ ();
   	return !wr_Mresp_SOF; 
 endmethod 
 method Bool outputs_mresp_EOF_ ();
    if (wr_Mresp_SOF == True)
	return !(True);
    else 
	return !(False);
 endmethod
 method Bool outputs_mresp_VLD_ ();
    if (wr_Mresp_SOF == True)
	return !(True);
    else 
	return !(False);
 endmethod 

 method Action _inputs_mresp_rdy_n_ (Bool value); // Input 
	wr_Mresp_rdy <= !(value);
 endmethod 

 //-- Maintenance Response Data Interface
/*
-- Output Signals are generated only when Maintenance Response signal is valid. 
*/
 method TT outputs_mresp_tt_ ();
//    if (wr_Mresp_SOF == True)
	return rg_Mresp_tt;
//    else 
//	return 0;
 endmethod 
 method Data outputs_mresp_data_ ();
//    let lv_mresp_data = fromMaybe (0, wr_RFDataOut);
    if (wr_Mresp_SOF == True)
//	return {wr_RFDataOut, 32'h00000000};
	return fn_ByteEnDataGeneration ({wr_RFDataOut, 32'd0}, rg_Mreq_byte_en);
    else 
	return 0;
 endmethod
 method Bool outputs_mresp_crf_ ();
	return False; 
 endmethod 
 method Prio outputs_mresp_prio_ ();
    if (wr_Mresp_SOF == True)
	return rg_Mresp_Prio;
    else 
	return 0;
 endmethod 
 method Type outputs_mresp_ftype_ ();
    if (wr_Mresp_SOF == True)
	return 4'h8;
    else 
	return 0;
 endmethod 
 method Type outputs_mresp_ttype_ ();
    if (wr_Mresp_SOF == True)
	return rg_TtypeResp;
    else 
	return 0;
 endmethod 
 method DestId outputs_mresp_dest_id_ ();
    if (wr_Mresp_SOF == True)
	return rg_Mreq_sourceid;
    else 
	return 0;
 endmethod 
 method Bit#(8) outputs_mresp_hop_count_ ();
	return 0; 
 endmethod
 method TranId outputs_mresp_tid_ ();
    if (wr_Mresp_SOF == True)
	return rg_Mreq_tid;
    else 
	return 0;
 endmethod 
 method Bool outputs_mresp_local_ ();
	return False; 
 endmethod 
 method Status outputs_mresp_status_ ();
	return wr_Status; 
 endmethod 
endmodule : mkRapidIO_RegisterManager

endpackage : RapidIO_RegisterManager
